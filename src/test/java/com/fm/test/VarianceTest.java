package com.fm.test;

import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.util.MathUtils;

public class VarianceTest {

	public static void main(String args[]){
		double[] arr={0.001,0.002,-0.001,-0.003,0.001,0.002,0.002,-0.001,-0.002,0.0};
		double variance =StatUtils.variance(arr);
		StandardDeviation sd=new StandardDeviation();
		System.out.println(sd.evaluate(arr));
		double deviation=Math.sqrt(variance);
		double sigma_2=2*deviation;
		double sigma_3=3*deviation;
		
		System.out.println(deviation);
		System.out.println(sigma_2);
		System.out.println(sigma_3);
	}
}
