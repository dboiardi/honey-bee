package com.fm.fixconnector.seb;

import java.util.Date;

import org.apache.camel.dataformat.bindy.annotation.KeyValuePairField;
import org.apache.camel.dataformat.bindy.annotation.Message;
import org.springframework.util.StringUtils;

import com.fm.fixconnector.domain.AbstractTradeCapture;
import com.fm.fixconnector.util.Constants;

@Message(pairSeparator = "\\u0001",keyValuePairSeparator = "=", type = "FIX", version = "4.3")
public class ExecutionReportSEB implements AbstractTradeCapture {
	@KeyValuePairField(tag = 37)
	private String marketOrderId;

	@KeyValuePairField(tag = 11)
	private String clientOrderId;

	@KeyValuePairField(tag = 17)
	private String execId;
	
	@KeyValuePairField(tag = 39)
	private String orderStatus;

	@KeyValuePairField(tag = 58)
	private String orderRejReason;

	@KeyValuePairField(tag = 1)
	private String account;

	@KeyValuePairField(tag = 55)
	private String symbol;

	

	@KeyValuePairField(tag = 15)
	private String currency;

	@KeyValuePairField(tag = 14)
	private String cumQty;

	@KeyValuePairField(tag = 6)
	private String avgPx;

	@KeyValuePairField(tag = 54)
	private String side;
	

	@KeyValuePairField(tag = 44)
	private String price;

	@KeyValuePairField(tag = 60)
	private String transactitionTime;

	public String getMarketOrderId() {
		return marketOrderId;
	}

	public void setMarketOrderId(String marketOrderId) {
		this.marketOrderId = marketOrderId;
	}

	public String getClientOrderId() {
		return clientOrderId;
	}

	public void setClientOrderId(String clientOrderId) {
		this.clientOrderId = clientOrderId;
	}

	public String getExecId() {
		return execId;
	}

	public void setExecId(String execId) {
		this.execId = execId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getOrderRejReason() {
		return orderRejReason;
	}

	public void setOrderRejReason(String orderRejReason) {
		this.orderRejReason = orderRejReason;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCumQty() {
		return cumQty;
	}

	public void setCumQty(String cumQty) {
		this.cumQty = cumQty;
	}

	public String getAvgPx() {
		return avgPx;
	}

	public void setAvgPx(String avgPx) {
		this.avgPx = avgPx;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public double getPrice() {
		if(StringUtils.hasText(price)){
			return Double.parseDouble(price);	
		}else{
			return 0.0;
		}
		
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTransactitionTime() {
		return transactitionTime;
	}

	public void setTransactitionTime(String transactitionTime) {
		this.transactitionTime = transactitionTime;
	}

	public String getOrderId() {
		// TODO Auto-generated method stub
		return clientOrderId;
	}

	public int getQunantity() {
		// TODO Auto-generated method stub
		if(StringUtils.hasText(cumQty)){
			return Integer.parseInt(cumQty);	
		}else{
			return 0;
		}
		
	}

	public Date getTradeDate() {
		// TODO Auto-generated method stub
		return new Date();
	}

	public String getCurrencyPair() {
		// TODO Auto-generated method stub
		return symbol;
	}

	public String getExchangeName() {
		// TODO Auto-generated method stub
		return Constants.SEB;
	}

	public String getOrderRejectRason() {
		// TODO Auto-generated method stub
		return orderRejReason;
	}

	public String getOrderType() {
		
		return side;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ExecutionReportSEB [ "+getExchangeName()+" ");
		if (marketOrderId != null) {
			builder.append("marketOrderId=");
			builder.append(marketOrderId);
			builder.append(", ");
		}
		if (clientOrderId != null) {
			builder.append("clientOrderId=");
			builder.append(clientOrderId);
			builder.append(", ");
		}
		if (execId != null) {
			builder.append("execId=");
			builder.append(execId);
			builder.append(", ");
		}
		if (orderStatus != null) {
			builder.append("orderStatus=");
			builder.append(orderStatus);
			builder.append(", ");
		}
		if (orderRejReason != null) {
			builder.append("orderRejReason=");
			builder.append(orderRejReason);
			builder.append(", ");
		}
		if (account != null) {
			builder.append("account=");
			builder.append(account);
			builder.append(", ");
		}
		if (symbol != null) {
			builder.append("symbol=");
			builder.append(symbol);
			builder.append(", ");
		}
		if (currency != null) {
			builder.append("currency=");
			builder.append(currency);
			builder.append(", ");
		}
		if (cumQty != null) {
			builder.append("cumQty=");
			builder.append(cumQty);
			builder.append(", ");
		}
		if (avgPx != null) {
			builder.append("avgPx=");
			builder.append(avgPx);
			builder.append(", ");
		}
		if (side != null) {
			builder.append("side=");
			builder.append(side);
			builder.append(", ");
		}
		if (price != null) {
			builder.append("price=");
			builder.append(price);
			builder.append(", ");
		}
		if (transactitionTime != null) {
			builder.append("transactitionTime=");
			builder.append(transactitionTime);
		}
		builder.append("]");
		return builder.toString();
	}

	public String getOrderSide() {
		if("1".equalsIgnoreCase(side))
			return "BUY";
					
			else if("2".equalsIgnoreCase(side)) return "SELL";
			else return "";
	}

}
