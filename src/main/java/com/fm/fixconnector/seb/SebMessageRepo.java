package com.fm.fixconnector.seb;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import quickfix.Group;
import quickfix.Message;
import quickfix.field.MsgType;

import com.fm.fixconnector.MessageRepo;
import com.fm.fixconnector.domain.AbstractMarketData;
import com.fm.fixconnector.util.Constants;

public class SebMessageRepo extends MessageRepo {

	public  Message getQuoteMessage(Map<Integer,String> valueMap){
		
		Message message=factory.create(getMessageVersion(), MsgType.QUOTE_REQUEST);
		message.getHeader().setString(49, getSenderCompId());
		message.getHeader().setString(50, valueMap.get(50));
		message.getHeader().setString(56, getTargetCompId());
		message.setString(131, ""+getRandomNo());
		message.setString(1, valueMap.get(1));
		message.setString(57, valueMap.get(57));
		Calendar calendar=Calendar.getInstance(TimeZone.getTimeZone("Europe/Berlin"));
		Date d=calendar.getTime();
		//message.setUtcTimeStamp(52, d);
		int a[]={55,54,38,60,15};
		Group group=new Group(146, 15,a);
		group.setString(55, valueMap.get(55));
		group.setString(54, valueMap.get(54));
		group.setString(38, valueMap.get(38));
		group.setUtcTimeStamp(60, d);
		group.setString(15, valueMap.get(55).split("/")[0]);
		message.addGroup(group);
		message.setString(263, valueMap.get(263));
		return message;
	}
	
	public  Message getQuoteOrderMessage(Map<Integer,String> valueMap,AbstractMarketData marketData){
		
		Message message=factory.create(getMessageVersion(), MsgType.ORDER_SINGLE);
		message.getHeader().setString
		(49, getSenderCompId());
		message.getHeader().setString(50, valueMap.get(50));
		message.getHeader().setString(56, getTargetCompId());
		message.setString(11, valueMap.get(Constants.ID));
		message.setString(1, valueMap.get(1));
		message.setString(21, "1");
		message.setString(57, valueMap.get(1));
		
		message.setString(55, marketData.getSymbol());
		int orderSide=0;
		if("1".equalsIgnoreCase(marketData.getPriceType())){
			orderSide=1;
		}else if("2".equalsIgnoreCase(marketData.getPriceType())){
			orderSide=2;
		}else{
			
		}
		
		message.setInt(54, orderSide);
		message.setString(38, valueMap.get(38));
		message.setDouble(44, marketData.getPrice());
		//message.setString(40, "2");
		message.setString(40, valueMap.get(40));
		message.setString(59, valueMap.get(59));
		message.setString(64, valueMap.get(64));
		message.setString(117, marketData.getQuoteId());
		message.setString(15, marketData.getSymbol().split("/")[0]);
		Calendar calendar=Calendar.getInstance(TimeZone.getTimeZone("Europe/Berlin"));
		Date d=calendar.getTime();
		message.setUtcTimeStamp(60, d);
		return message;
	}
}
