package com.fm.fixconnector.nordea;

import org.apache.camel.dataformat.bindy.annotation.KeyValuePairField;
import org.apache.camel.dataformat.bindy.annotation.Message;

@Message(pairSeparator = "\\u0001",keyValuePairSeparator = "=",  type="FIX", version="4.1")
public class ExecutionReport {

	@KeyValuePairField(tag = 37)
	private String   marketOrderId;
	
	@KeyValuePairField(tag = 11)
	private String   clientOrderId;
	
	@KeyValuePairField(tag = 39)
	private String   orderStatus;
	
	@KeyValuePairField(tag = 103)
	private String   orderRejReason;

	@KeyValuePairField(tag = 55)
	private String   symbol;
	
	@KeyValuePairField(tag = 40)
	private String   orderType;
	
	@KeyValuePairField(tag = 15)
	private String   currecny;
	
	@KeyValuePairField(tag = 194)
	private String   fxSpotRate;
	
	@KeyValuePairField(tag = 54)
	private String   side;
	

	@KeyValuePairField(tag = 60)
	private String   transactitionTime;


	public String getMarketOrderId() {
		return marketOrderId;
	}


	public void setMarketOrderId(String marketOrderId) {
		this.marketOrderId = marketOrderId;
	}


	public String getClientOrderId() {
		return clientOrderId;
	}


	public void setClientOrderId(String clientOrderId) {
		this.clientOrderId = clientOrderId;
	}


	public String getOrderStatus() {
		return orderStatus;
	}


	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}


	public String getOrderRejReason() {
		return orderRejReason;
	}


	public void setOrderRejReason(String orderRejReason) {
		this.orderRejReason = orderRejReason;
	}


	public String getSymbol() {
		return symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public String getOrderType() {
		return orderType;
	}


	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}


	public String getCurrecny() {
		return currecny;
	}


	public void setCurrecny(String currecny) {
		this.currecny = currecny;
	}


	public String getFxSpotRate() {
		return fxSpotRate;
	}


	public void setFxSpotRate(String fxSpotRate) {
		this.fxSpotRate = fxSpotRate;
	}


	public String getSide() {
		return side;
	}


	public void setSide(String side) {
		this.side = side;
	}


	public String getTransactitionTime() {
		return transactitionTime;
	}


	public void setTransactitionTime(String transactitionTime) {
		this.transactitionTime = transactitionTime;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ExecutionReport [");
		if (marketOrderId != null) {
			builder.append("marketOrderId=");
			builder.append(marketOrderId);
			builder.append(", ");
		}
		if (clientOrderId != null) {
			builder.append("clientOrderId=");
			builder.append(clientOrderId);
			builder.append(", ");
		}
		if (orderStatus != null) {
			builder.append("orderStatus=");
			builder.append(orderStatus);
			builder.append(", ");
		}
		if (orderRejReason != null) {
			builder.append("orderRejReason=");
			builder.append(orderRejReason);
			builder.append(", ");
		}
		if (symbol != null) {
			builder.append("symbol=");
			builder.append(symbol);
			builder.append(", ");
		}
		if (orderType != null) {
			builder.append("orderType=");
			builder.append(orderType);
			builder.append(", ");
		}
		if (currecny != null) {
			builder.append("currecny=");
			builder.append(currecny);
			builder.append(", ");
		}
		if (fxSpotRate != null) {
			builder.append("fxSpotRate=");
			builder.append(fxSpotRate);
			builder.append(", ");
		}
		if (side != null) {
			builder.append("side=");
			builder.append(side);
			builder.append(", ");
		}
		if (transactitionTime != null) {
			builder.append("transactitionTime=");
			builder.append(transactitionTime);
		}
		builder.append("]");
		return builder.toString();
	}
	
	
	

}
