package com.fm.fixconnector.nordea;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.camel.dataformat.bindy.annotation.KeyValuePairField;
import org.apache.camel.dataformat.bindy.annotation.Link;
import org.apache.camel.dataformat.bindy.annotation.Message;
import org.apache.camel.dataformat.bindy.annotation.Section;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.util.StringUtils;

import com.fm.fixconnector.domain.AbstractMarketData;

@Message(pairSeparator = "\\u0001",keyValuePairSeparator = "=", type = "FIX", version = "4.4")
public class MarketData{

	/*
	 * @Link private Header header;
	 * 
	 * @Link private Trailer trailer;
	 */
	@KeyValuePairField(tag = 299)
	private String quoteEntryID;

	@KeyValuePairField(tag = 52)
	private String sendingTime;

	
	@KeyValuePairField(tag = 282)
	private String mdEntryOriginator;

	@KeyValuePairField(tag = 270)
	private double price;
	
	@KeyValuePairField(tag = 279)
	private int mdUpdateAction;

	@KeyValuePairField(tag = 269)
	private String priceType;
	
	@KeyValuePairField(tag = 15)
	private String currency;
	
	@KeyValuePairField(tag = 55)
	private String symbol;

	@KeyValuePairField(tag = 271) 
	private long quantity;
	
	@KeyValuePairField(tag = 278) 
	private String mdEntryID;

	@KeyValuePairField(tag = 262) 
	private String mdReqId;
	
	private DateTime serverTime;
	
	private DateTime clientTime;
	
	
	public DateTime getServerTime() {
		return serverTime;
	}

	public void setServerTime(DateTime serverTime) {
		this.serverTime = serverTime;
	}

	public DateTime getClientTime() {
		return clientTime;
	}

	public void setClientTime(DateTime clientTime) {
		this.clientTime = clientTime;
	}

	public String getQuoteEntryID() {
		return quoteEntryID;
	}

	public void setQuoteEntryID(String quoteEntryID) {
		this.quoteEntryID = quoteEntryID;
	}

	public String getSendingTime() {
		return sendingTime;
	}
	
	
	public void setSendingTime(String sendingTime) {
		this.sendingTime = sendingTime;
	}

	public String getMdEntryOriginator() {
		return mdEntryOriginator;
	}

	public void setMdEntryOriginator(String mdEntryOriginator) {
		this.mdEntryOriginator = mdEntryOriginator;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getMdUpdateAction() {
		return mdUpdateAction;
	}

	public void setMdUpdateAction(int mdUpdateAction) {
		this.mdUpdateAction = mdUpdateAction;
	}

	public String getPriceType() {
		return priceType;
	}

	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

	public String getCurrency() {
		if(StringUtils.hasText(currency)){
			return currency;
		}else{
			return mdReqId.split("#")[0];
		}
		
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public String getMdEntryID() {
		return mdEntryID;
	}

	public void setMdEntryID(String mdEntryID) {
		this.currency=mdReqId.split("#")[0];
		this.mdEntryID = mdEntryID;
	}

	@Override
	public String toString() {
		return "MarketData ["
				+ (quoteEntryID != null ? "quoteEntryID=" + quoteEntryID + ", "
						: "")
				+ (sendingTime != null ? "sendingTime=" + sendingTime + ", "
						: "")
				+ (mdEntryOriginator != null ? "mdEntryOriginator="
						+ mdEntryOriginator + ", " : "") + "price=" + price
				+ ", mdUpdateAction=" + mdUpdateAction + ", "
				+ (priceType != null ? "priceType=" + priceType + ", " : "")
				+ (currency != null ? "currency=" + currency + ", " : "")
				+ (symbol != null ? "symbol=" + symbol + ", " : "")
				+ "quantity=" + quantity + ", mdEntryID=" + mdEntryID + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
	
		return HashCodeBuilder.reflectionHashCode(this);
	}

	
	}
