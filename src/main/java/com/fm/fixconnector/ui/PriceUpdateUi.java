package com.fm.fixconnector.ui;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;

import com.fm.fixconnector.MessageController;
import com.fm.fixconnector.domain.PriceType;
import com.fm.fixconnector.domain.PriceUpdate;
import com.fm.fixconnector.domain.Strategy;
import com.fm.fixconnector.domain.StrategyCurrencyPair;
import com.fm.fixconnector.domain.StrategyUpdate;
import com.fm.fixconnector.strategy.StrategyCalculationThread;
import com.fm.fixconnector.util.Constants;
import com.sun.tools.jdi.EventSetImpl.Itr;

public class PriceUpdateUi {
	
	private static ObjectMapper objectMapper=new ObjectMapper();
	static final Logger logger = Logger.getLogger(PriceUpdateUi.class);
	private MessageController controller;
	private ConcurrentHashMap<String , StrategyUpdate> priceUpdates=new ConcurrentHashMap<>();
	private final ScheduledExecutorService seduleexecuter=Executors.newScheduledThreadPool(1);

	
	public void init(){
		final Runnable thread=new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Set set=priceUpdates.entrySet();
				Iterator iterator=set.iterator();
				while(iterator.hasNext()) {
		 	          Map.Entry mEntry = (Map.Entry)iterator.next();
		 	         try {
		 				String jString=objectMapper.writeValueAsString(mEntry.getValue());
		 				logger.debug("Sending following messages to UI "+jString);
		 				// strategyUpdateRepo.addStrategyValue(strategyUpdate);
		 				controller.sendMessage(jString);
		 				
		 			} catch (JsonGenerationException e) {
		 				e.printStackTrace();
		 			} catch (JsonMappingException e) {
		 				e.printStackTrace();
		 			} catch (IOException e) {
		 				e.printStackTrace();
		 			}
		 	       }
				
			}
		};
		seduleexecuter.scheduleWithFixedDelay(thread, 0, 1000, TimeUnit.MILLISECONDS);	  
		
	}
	
	public void sendUpdateToUi(PriceUpdate priceUpdate,double quotedValue,double baseValue,PriceType priceType,double startSizeBID,double startSizeASK,StrategyCurrencyPair strategyCurrencyPair,DateTime updateTime,Strategy strategy){
		
		String fromCurrency=priceType.BID==priceType?priceUpdate.getCurrencyPair().split(Constants.CURR_SEPRATOR)[0]:priceUpdate.getCurrencyPair().split(Constants.CURR_SEPRATOR)[1];
		String toCurrency=priceType.BID==priceType?priceUpdate.getCurrencyPair().split(Constants.CURR_SEPRATOR)[1]:priceUpdate.getCurrencyPair().split(Constants.CURR_SEPRATOR)[0];
		StrategyUpdate strategyUpdate = new StrategyUpdate(strategy.getName(),priceUpdate, baseValue, quotedValue,fromCurrency,toCurrency,startSizeBID,startSizeASK,strategyCurrencyPair,strategy,updateTime);
		insert(strategy.getName(),strategyUpdate);
			
	}
	
	public void insert(String key,StrategyUpdate strategyUpdate){
		
	
			//String jString=objectMapper.writeValueAsString(object);
			//logger.debug("Sending following messages to UI "+jString);
			// strategyUpdateRepo.addStrategyValue(strategyUpdate);
			//controller.sendMessage(jString);
			priceUpdates.put(key, strategyUpdate);
			
	
		
	}

	public MessageController getController() {
		return controller;
	}

	public void setController(MessageController controller) {
		this.controller = controller;
	}
	
	
	
}
