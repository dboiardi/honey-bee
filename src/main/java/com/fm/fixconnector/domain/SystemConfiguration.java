package com.fm.fixconnector.domain;

public class SystemConfiguration {

	private String id;
	private double profit;
	private long priceReplaceTimeInMS;
	
	public SystemConfiguration(double profit, long priceReplaceTimeInMS) {
		super();
		this.profit = profit;
		this.priceReplaceTimeInMS = priceReplaceTimeInMS;
	}
	
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public double getProfit() {
		return profit;
	}
	public void setProfit(double profit) {
		this.profit = profit;
	}
	public long getPriceReplaceTimeInMS() {
		return priceReplaceTimeInMS;
	}
	public void setPriceReplaceTimeInMS(long priceReplaceTimeInMS) {
		this.priceReplaceTimeInMS = priceReplaceTimeInMS;
	}


	@Override
	public String toString() {
		return "SystemConfiguration [" + (id != null ? "id=" + id + ", " : "")
				+ "profit=" + profit + ", priceReplaceTimeInMS="
				+ priceReplaceTimeInMS + "]";
	}
	
	
	
}
