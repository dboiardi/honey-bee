package com.fm.fixconnector.domain;

import org.apache.camel.dataformat.bindy.annotation.KeyValuePairField;
import org.apache.camel.dataformat.bindy.annotation.Message;

@Message(pairSeparator = "\\u0001",keyValuePairSeparator = "=", type = "FIX", version = "4.4" )
public class BrokerExecutionReport {

	@KeyValuePairField(tag = 375)
	private String contraBroker;
	
	@KeyValuePairField(tag = 437)
	private int tradeQty;
	@KeyValuePairField(tag = 438)
	private String tradeTime;
	@KeyValuePairField(tag = 10003)
	private String tradeId;
	@KeyValuePairField(tag = 10005)
	private long tradeTimeBracket;
	
	@KeyValuePairField(tag = 10006)
	private double tradeExecutionPrice;
	
	@KeyValuePairField(tag = 10019)
	private String tradeConfirmationTicket;
	
	@KeyValuePairField(tag = 150)
	private String execType;
	
	@KeyValuePairField(tag = 64)
	private String settleDate;

	public String getContraBroker() {
		return contraBroker;
	}

	public void setContraBroker(String contraBroker) {
		this.contraBroker = contraBroker;
	}

	public int getTradeQty() {
		return tradeQty;
	}

	public void setTradeQty(int tradeQty) {
		this.tradeQty = tradeQty;
	}

	public String getTradeTime() {
		return tradeTime;
	}

	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}

	public String getTradeId() {
		return tradeId;
	}

	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}

	public long getTradeTimeBracket() {
		return tradeTimeBracket;
	}

	public void setTradeTimeBracket(long tradeTimeBracket) {
		this.tradeTimeBracket = tradeTimeBracket;
	}

	public double getTradeExecutionPrice() {
		return tradeExecutionPrice;
	}

	public void setTradeExecutionPrice(double tradeExecutionPrice) {
		this.tradeExecutionPrice = tradeExecutionPrice;
	}

	public String getTradeConfirmationTicket() {
		return tradeConfirmationTicket;
	}

	public void setTradeConfirmationTicket(String tradeConfirmationTicket) {
		this.tradeConfirmationTicket = tradeConfirmationTicket;
	}

	public String getExecType() {
		return execType;
	}

	public void setExecType(String execType) {
		this.execType = execType;
	}

	public String getSettleDate() {
		return settleDate;
	}

	public void setSettleDate(String settleDate) {
		this.settleDate = settleDate;
	}
	
	
	
}
