package com.fm.fixconnector.domain;

import org.apache.camel.dataformat.bindy.annotation.KeyValuePairField;
import org.apache.camel.dataformat.bindy.annotation.Message;


@Message(pairSeparator = "\\u0001",keyValuePairSeparator = "=", type = "FIX", version = "4.4" )
public class TradeableSecurity {

	private String id;
	
	@KeyValuePairField(tag = 55)
	private String symbol;
	@KeyValuePairField(tag = 561)
	private Long roundLot;
	@KeyValuePairField(tag = 562)
	private Long minTradeVol;
	@KeyValuePairField(tag = 10001)
	private int fractionSize;
	@KeyValuePairField(tag = 10002)
	private String extraDigitSupport;
	
	private Boolean active=false;
	
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public Long getRoundLot() {
		return roundLot;
	}
	public void setRoundLot(Long roundLot) {
		this.roundLot = roundLot;
	}
	public Long getMinTradeVol() {
		return minTradeVol;
	}
	public void setMinTradeVol(Long minTradeVol) {
		this.minTradeVol = minTradeVol;
	}
	public int getFractionSize() {
		return fractionSize;
	}
	public void setFractionSize(int fractionSize) {
		this.fractionSize = fractionSize;
	}
	public String getExtraDigitSupport() {
		return extraDigitSupport;
	}
	public void setExtraDigitSupport(String extraDigitSupport) {
		this.extraDigitSupport = extraDigitSupport;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "TradeableSecurity ["
				+ (symbol != null ? "symbol=" + symbol + ", " : "")
				+ (roundLot != null ? "roundLot=" + roundLot + ", " : "")
				+ (minTradeVol != null ? "minTradeVol=" + minTradeVol + ", "
						: "")
				+ "fractionSize="
				+ fractionSize
				+ ", "
				+ (extraDigitSupport != null ? "extraDigitSupport="
						+ extraDigitSupport : "") + "]";
	}
	
	
	
}
