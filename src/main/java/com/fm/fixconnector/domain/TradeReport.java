package com.fm.fixconnector.domain;

import java.util.HashMap;
import java.util.List;

import org.joda.time.DateTime;

public class TradeReport {

	private String stratgeyName;
	private DateTime tradetime;
	private double totalProfit;
	private double desireProfit;
	private String reportingCurrency;
	private double profitInReportingCurrency;
	
	private HashMap<String, Double> priceValueMap=new HashMap<>();
	private HashMap<String, Long> sizeValueMap=new HashMap<>();
	private HashMap<String, String> lpValueMap=new HashMap<>();
	
	public TradeReport(String stratgeyName, DateTime tradetime,
			double totalProfit, double desireProfit,
			HashMap<String, Double> priceValueMap,HashMap<String, Long> sizeValueMap,HashMap<String, String> lpValueMap,
			String reportingCurrency,double profitInReportingCurrency) {
		super();
		this.stratgeyName = stratgeyName;
		this.tradetime = tradetime;
		this.totalProfit = totalProfit;
		this.desireProfit = desireProfit;
		this.priceValueMap = priceValueMap;
		this.sizeValueMap=sizeValueMap;
		this.lpValueMap=lpValueMap;
		this.reportingCurrency=reportingCurrency;
		this.profitInReportingCurrency=profitInReportingCurrency;
		
	}
	
	public String getStratgeyName() {
		return stratgeyName;
	}
	public void setStratgeyName(String stratgeyName) {
		this.stratgeyName = stratgeyName;
	}
	public DateTime getTradetime() {
		return tradetime;
	}
	public void setTradetime(DateTime tradetime) {
		this.tradetime = tradetime;
	}
	public double getTotalProfit() {
		return totalProfit;
	}
	public void setTotalProfit(double totalProfit) {
		this.totalProfit = totalProfit;
	}
	public double getDesireProfit() {
		return desireProfit;
	}
	public void setDesireProfit(double desireProfit) {
		this.desireProfit = desireProfit;
	}
	public HashMap<String, Double> getPriceValueMap() {
		return priceValueMap;
	}
	public void setPriceValueMap(HashMap<String, Double> priceValueMap) {
		this.priceValueMap = priceValueMap;
	}
	public HashMap<String, Long> getSizeValueMap() {
		return sizeValueMap;
	}
	public void setSizeValueMap(HashMap<String, Long> sizeValueMap) {
		this.sizeValueMap = sizeValueMap;
	}
	public HashMap<String, String> getLpValueMap() {
		return lpValueMap;
	}
	public void setLpValueMap(HashMap<String, String> lpValueMap) {
		this.lpValueMap = lpValueMap;
	}
	public String getReportingCurrency() {
		return reportingCurrency;
	}
	public void setReportingCurrency(String reportingCurrency) {
		this.reportingCurrency = reportingCurrency;
	}
	public double getProfitInReportingCurrency() {
		return profitInReportingCurrency;
	}
	
	public void setProfitInReportingCurrency(double profitInReportingCurrency) {
		this.profitInReportingCurrency = profitInReportingCurrency;
	}
	
	
	
}
