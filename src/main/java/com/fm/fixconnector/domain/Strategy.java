package com.fm.fixconnector.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.joda.time.DateTime;

public class Strategy {

	private String id;
	private int stratId;
	private String name;
	private boolean isActive;
	private List<StrategyCurrencyPair> securitys;
	private DateTime createdTime = new DateTime();
	private double amount;
	private boolean isStreamingRequired=true;
	private String tradingcurrency;
	private int tradingLimit;
	private String reportingCurrencyPair;
	private PriceType reportingCurrencypriceType;
	private String reportingCurrency;
	
	private List<String> secCurrency = new ArrayList<>();

	public Strategy(String name, boolean isActive) {
		super();
		this.name = name;
		this.isActive = isActive;

	}
	
	public PriceType getReportingCurrencypriceType() {
		return reportingCurrencypriceType;
	}

	public void setReportingCurrencypriceType(PriceType reportingCurrencypriceType) {
		this.reportingCurrencypriceType = reportingCurrencypriceType;
	}

	public String getReportingCurrencyPair() {
		return reportingCurrencyPair;
	}


	public void setReportingCurrencyPair(String reportingCurrencyPair) {
		this.reportingCurrencyPair = reportingCurrencyPair;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getSecCurrency() {
		return secCurrency;
	}

	public void setSecCurrency(List<String> secCurrency) {
		this.secCurrency = secCurrency;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public DateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(DateTime createdTime) {
		this.createdTime = createdTime;
	}

	public int getStratId() {
		return stratId;
	}

	public void setStratId(int stratId) {
		this.stratId = stratId;
	}

	public List<StrategyCurrencyPair> getSecuritys() {
		return securitys;
	}

	public List<String> getSecuritysCurrencyPair() {
		return secCurrency;
	}

	
	public String getTradingcurrency() {
		return tradingcurrency;
	}

	public void setTradingcurrency(String tradingcurrency) {
		this.tradingcurrency = tradingcurrency;
	}

	public int getTradingLimit() {
		return tradingLimit;
	}

	public void setTradingLimit(int tradingLimit) {
		this.tradingLimit = tradingLimit;
	}
	
	public String getReportingCurrency() {
		return reportingCurrency;
	}

	public void setReportingCurrency(String reportingCurrency) {
		this.reportingCurrency = reportingCurrency;
	}

	public void setSecuritys(List<StrategyCurrencyPair> securitys) {
		Collections.sort(securitys, new Comparator<StrategyCurrencyPair>() {

			@Override
			public int compare(StrategyCurrencyPair o1, StrategyCurrencyPair o2) {
				return o1.getSequenceNo() - o2.getSequenceNo();
			}
		});

		this.securitys = securitys;
		secCurrency = new ArrayList<String>();

		for (StrategyCurrencyPair stratCurr : securitys) {
			secCurrency.add(stratCurr.getCurrencyPair());
		}

	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
	
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
