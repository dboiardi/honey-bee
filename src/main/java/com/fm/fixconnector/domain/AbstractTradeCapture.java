package com.fm.fixconnector.domain;

import java.util.Date;

public interface AbstractTradeCapture {

	public String getOrderId();
	public int getQunantity();
	public double getPrice();
	public Date getTradeDate();
	public String getCurrencyPair();
	public String getExchangeName();
	public String getOrderStatus();
	public String getOrderRejectRason();
	public String getOrderType();
	public String getOrderSide();
}
