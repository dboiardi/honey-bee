package com.fm.fixconnector.domain;

import java.util.List;

import org.apache.camel.dataformat.bindy.annotation.KeyValuePairField;
import org.apache.camel.dataformat.bindy.annotation.Message;
import org.apache.camel.dataformat.bindy.annotation.OneToMany;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;


@Message(pairSeparator = "\\u0001",keyValuePairSeparator = "=", type = "FIX", version = "4.4")
public class SecurityList {

	@OneToMany(mappedTo = "com.fm.fixconnector.domain.TradeableSecurity")
	private List<TradeableSecurity> securitys;

	public List<TradeableSecurity> getSecuritys() {
		return securitys;
	}

	public void setSecuritys(List<TradeableSecurity> securitys) {
		this.securitys = securitys;
	}

	@Override
	public String toString() {
		return "SecurityList ["
				+ (securitys != null ? "securitys=" + securitys : "") + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
	
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
}
