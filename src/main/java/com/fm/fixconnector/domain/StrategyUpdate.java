package com.fm.fixconnector.domain;

import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.NumberUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.joda.time.DateTime;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import com.fm.fixconnector.util.Constants;

public class StrategyUpdate {

	private String strategyName;
	private int seqNo;
	private String currencyPair;
	private double bidPrice;
	private double askPrice;
	private double spread; // Up to 5 decimal Place
	private String bidSize; //done String with Comma
	private String askSize; //done String with Comma
	private String bestBidSize; // String with Comma
	private String bestAskSize;// String with Comma
	
	private String startBidSize;// String with Comma
	private String startAskSize;// String with Comma
	
	private String tradingLimit; // String with Comma
	private String tradingLimitCurr;
	
	private String startBidCurr;
	private String startAskCurr;
	private String bestBidLP;
	private String bestAskLP;
	private int sequence;
	private String baseCurrencyValuation; // String with Comma
	private String quoteCurrencyValuation; // String with Comma
	private String baseCurrency;
	private String quoteCurrency;
	private double grossProfitLoss; 
	private double tradeCost;
	private double netProfitLoss;
	private int noOfTrades;
	private DateTime updateTime;
	
	public StrategyUpdate() {
		super();
	}
	
	public StrategyUpdate(String strategyName, String currencyPair,
			double bidPrice, double askPrice, double spread, double bidSize,
			double askSize, double baseCurrencyValuation,
			double quoteCurrencyValuation, String baseCurrency,
			String quoteCurrency,double startBidSize,double startAskSize) {
		super();
		this.strategyName = strategyName;
		this.currencyPair = currencyPair;
		this.bidPrice = bidPrice;
		this.askPrice = askPrice;
		this.spread = spread;
		this.bidSize = NumberFormat.getNumberInstance(Locale.US).format(bidSize);
		this.askSize = NumberFormat.getNumberInstance(Locale.US).format(askSize);
		this.baseCurrencyValuation =NumberFormat.getNumberInstance(Locale.US).format(baseCurrencyValuation) ;
		this.quoteCurrencyValuation = NumberFormat.getNumberInstance(Locale.US).format(quoteCurrencyValuation);
		this.baseCurrency = baseCurrency;
		this.quoteCurrency = quoteCurrency;
		this.startBidSize=NumberFormat.getNumberInstance(Locale.US).format(startBidSize);
		this.startAskSize=NumberFormat.getNumberInstance(Locale.US).format(startAskSize);
		
	}
	// This need to be chnage
	public StrategyUpdate(String strategyName,PriceUpdate priceUpdate , double baseCurrencyValuation,
			double quoteCurrencyValuation,String fromCurrency,String toCurrency,double startBidSize,
			double startAskSize,StrategyCurrencyPair strategyCurrencyPair,Strategy strategy,DateTime updateTime) {
		super();
		this.strategyName = strategyName+"-"+strategyCurrencyPair.getSequenceNo();
		this.seqNo=strategyCurrencyPair.getSequenceNo();
		this.currencyPair = priceUpdate.getCurrencyPair();
		this.bidPrice = priceUpdate.getBestBidPrice();
		this.askPrice = priceUpdate.getBestAskPrice();
		this.spread = priceUpdate.getSpread();
		this.bidSize = NumberFormat.getNumberInstance(Locale.US).format(priceUpdate.getBidSize());
		this.bestAskSize=NumberFormat.getNumberInstance(Locale.US).format((int)priceUpdate.getBestaskSize());
		this.bestBidSize=NumberFormat.getNumberInstance(Locale.US).format((int)priceUpdate.getBestbidSize());
		this.askSize = NumberFormat.getNumberInstance(Locale.US).format(priceUpdate.getAskSize());
		this.baseCurrencyValuation =NumberFormat.getNumberInstance(Locale.US).format((int)baseCurrencyValuation) ;
		this.quoteCurrencyValuation = NumberFormat.getNumberInstance(Locale.US).format((int)quoteCurrencyValuation);
		this.baseCurrency = fromCurrency;
		this.quoteCurrency = toCurrency;
		this.startBidSize=NumberFormat.getNumberInstance(Locale.US).format((int)startBidSize);
		this.startAskSize=NumberFormat.getNumberInstance(Locale.US).format((int)startAskSize);
		this.tradingLimit=NumberFormat.getNumberInstance(Locale.US).format((int)strategyCurrencyPair.getAmount());
		this.tradingLimitCurr=strategyCurrencyPair.getTradingLimitCCY();
		this.startBidCurr=strategyCurrencyPair.getCurrencyPairToEval().split(Constants.CURR_SEPRATOR)[0];
		this.startAskCurr=strategyCurrencyPair.getCurrencyPairToEval().split(Constants.CURR_SEPRATOR)[0];
		this.bestAskLP= priceUpdate.getBestASkLP().toUpperCase();
		this.bestBidLP=priceUpdate.getBestBidLP().toUpperCase();
		this.sequence=strategyCurrencyPair.getSequenceNo();
		this.updateTime=updateTime;
	}
	
	
	public DateTime getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(DateTime updateTime) {
		this.updateTime = updateTime;
	}

	public String getStartBidSize() {
		return startBidSize;
	}

	public void setStartBidSize(String startBidSize) {
		this.startBidSize = startBidSize;
	}

	public String getStartAskSize() {
		return startAskSize;
	}
	public int getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	public void setStartAskSize(String startAskSize) {
		this.startAskSize = startAskSize;
	}

	public String getTradingLimit() {
		return tradingLimit;
	}

	public void setTradingLimit(String tradingLimit) {
		this.tradingLimit = tradingLimit;
	}

	public String getTradingLimitCurr() {
		return tradingLimitCurr;
	}

	public void setTradingLimitCurr(String tradingLimitCurr) {
		this.tradingLimitCurr = tradingLimitCurr;
	}

	public String getStartBidCurr() {
		return startBidCurr;
	}

	public void setStartBidCurr(String startBidCurr) {
		this.startBidCurr = startBidCurr;
	}

	public String getStartAskCurr() {
		return startAskCurr;
	}

	public void setStartAskCurr(String startAskCurr) {
		this.startAskCurr = startAskCurr;
	}

	public String getStrategyName() {
		return strategyName;
	}
	public void setStrategyName(String strategyName) {
		this.strategyName = strategyName;
	}
	public String getCurrencyPair() {
		return currencyPair;
	}
	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair;
	}
	public double getBidPrice() {
		return bidPrice;
	}
	public void setBidPrice(double bidPrice) {
		this.bidPrice = bidPrice;
	}
	public double getAskPrice() {
		return askPrice;
	}
	public void setAskPrice(double askPrice) {
		this.askPrice = askPrice;
	}
	public String getSpread() {
		String str = String.format("%.5f", spread);
		return str;
	}
	public void setSpread(double spread) {
		this.spread = spread;
	}
	public String getBidSize() {
		return bidSize;
	}
	public void setBidSize(String bidSize) {
		this.bidSize = bidSize;
	}
	public String getAskSize() {
		return askSize;
	}
	public void setAskSize(String askSize) {
		this.askSize = askSize;
	}
	
	
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	public String getQuoteCurrency() {
		return quoteCurrency;
	}
	public void setQuoteCurrency(String quoteCurrency) {
		this.quoteCurrency = quoteCurrency;
	}
	public double getGrossProfitLoss() {
		return grossProfitLoss;
	}
	public void setGrossProfitLoss(double grossProfitLoss) {
		this.grossProfitLoss = grossProfitLoss;
	}
	public double getTradeCost() {
		return tradeCost;
	}
	public void setTradeCost(double tradeCost) {
		this.tradeCost = tradeCost;
	}
	public double getNetProfitLoss() {
		return netProfitLoss;
	}
	public void setNetProfitLoss(double netProfitLoss) {
		this.netProfitLoss = netProfitLoss;
	}
	public int getNoOfTrades() {
		return noOfTrades;
	}
	public void setNoOfTrades(int noOfTrades) {
		this.noOfTrades = noOfTrades;
	}

	public String getBestBidSize() {
		return bestBidSize;
	}

	public void setBestBidSize(String bestBidSize) {
		this.bestBidSize = bestBidSize;
	}

	public String getBestAskSize() {
		return bestAskSize;
	}

	public void setBestAskSize(String bestAskSize) {
		this.bestAskSize = bestAskSize;
	}

	public String getBaseCurrencyValuation() {
		return baseCurrencyValuation;
	}

	public void setBaseCurrencyValuation(String baseCurrencyValuation) {
		this.baseCurrencyValuation = baseCurrencyValuation;
	}

	public String getQuoteCurrencyValuation() {
		return quoteCurrencyValuation;
	}

	public void setQuoteCurrencyValuation(String quoteCurrencyValuation) {
		this.quoteCurrencyValuation = quoteCurrencyValuation;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}
	public String getBestBidLP() {
		return bestBidLP;
	}
	public void setBestBidLP(String bestBidLP) {
		this.bestBidLP = bestBidLP;
	}
	public String getBestAskLP() {
		return bestAskLP;
	}
	public void setBestAskLP(String bestAskLP) {
		this.bestAskLP = bestAskLP;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
	
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
