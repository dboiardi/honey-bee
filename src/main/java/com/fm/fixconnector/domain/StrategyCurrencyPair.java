package com.fm.fixconnector.domain;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class StrategyCurrencyPair {

	private String stratName;
	private int sequenceNo;
	private String fromCurrency;
	private String toCurrency;
	private PriceType priceType;
	private Integer amount;
	private String currencyPair;
	private String isActive;
	private String currencyPairToEval;
	private PriceType evalPriceType;
	private String tradingLimitCCY;
	private int refreshRate;
	private String reportingCurrency;
	private PriceType reportingPriceType;
	private String reportingCurr;
	
	public StrategyCurrencyPair() {
		
	}
	
	public StrategyCurrencyPair(String stratName, int sequenceNo,
			String fromCurrency, String toCurrency, PriceType priceType,
			Integer amount, String currencyPair,String isActive) {
		super();
		this.stratName = stratName;
		this.sequenceNo = sequenceNo;
		this.fromCurrency = fromCurrency;
		this.toCurrency = toCurrency;
		this.priceType = priceType;
		this.amount = amount;
		this.currencyPair = currencyPair;
	}
	
	
	public String getReportingCurrency() {
		return reportingCurrency;
	}

	public void setReportingCurrency(String reportingCurrency) {
		this.reportingCurrency = reportingCurrency;
	}

	public PriceType getReportingPriceType() {
		return reportingPriceType;
	}

	public void setReportingPriceType(PriceType reportingPriceType) {
		this.reportingPriceType = reportingPriceType;
	}

	public int getRefreshRate() {
		return refreshRate;
	}

	public void setRefreshRate(int refreshRate) {
		this.refreshRate = refreshRate;
	}

	public String getCurrencyPairToEval() {
		return currencyPairToEval;
	}

	public void setCurrencyPairToEval(String currencyPairToEval) {
		this.currencyPairToEval = currencyPairToEval;
	}

	public PriceType getEvalPriceType() {
		return evalPriceType;
	}

	public void setEvalPriceType(PriceType evalPriceType) {
		this.evalPriceType = evalPriceType;
	}

	public String getStratName() {
		return stratName;
	}
	public void setStratName(String stratName) {
		this.stratName = stratName;
	}
	public String getCurrencyPair() {
		return currencyPair;
	}
	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair;
	}
	public int getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(int sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public String getFromCurrency() {
		return fromCurrency;
	}
	public void setFromCurrency(String fromCurrency) {
		this.fromCurrency = fromCurrency;
	}
	public String getToCurrency() {
		return toCurrency;
	}
	public void setToCurrency(String toCurrency) {
		this.toCurrency = toCurrency;
	}
	public PriceType getPriceType() {
		return priceType;
	}
	public void setPriceType(PriceType priceType) {
		this.priceType = priceType;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getTradingLimitCCY() {
		return tradingLimitCCY;
	}

	public void setTradingLimitCCY(String tradingLimitCCY) {
		this.tradingLimitCCY = tradingLimitCCY;
	}
	
	public String getReportingCurr() {
		return reportingCurr;
	}

	public void setReportingCurr(String reportingCurr) {
		this.reportingCurr = reportingCurr;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
	
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
