package com.fm.fixconnector.domain;

import org.apache.camel.dataformat.bindy.annotation.KeyValuePairField;
import org.apache.camel.dataformat.bindy.annotation.Message;
import org.apache.camel.dataformat.bindy.annotation.OneToMany;

import java.util.Date;
import java.util.List;

import org.apache.camel.dataformat.bindy.annotation.KeyValuePairField;
import org.apache.camel.dataformat.bindy.annotation.Message;

import com.fm.fixconnector.domain.AbstractTradeCapture;
import com.fm.fixconnector.util.Constants;

@Message(pairSeparator = "\\u0001", keyValuePairSeparator = "=", type = "FIX", version = "4.1")
public class TradeExecutionReport {

	private String id;

	@KeyValuePairField(tag = 37)
	private String marketOrderId;

	
	@OneToMany(mappedTo = "com.fm.fixconnector.domain.BrokerExecutionReport")
	private List<BrokerExecutionReport> brokerExeReport;
	
	@KeyValuePairField(tag = 11)
	private String clientOrderId;

	@KeyValuePairField(tag = 17)
	private String execId;

	@KeyValuePairField(tag = 54)
	private String side;

	@KeyValuePairField(tag = 6)
	private double avgPx;

	@KeyValuePairField(tag = 58)
	private double text;

	@KeyValuePairField(tag = 31)
	private double lastPX;

	@KeyValuePairField(tag = 55)
	private String symbol;

	@KeyValuePairField(tag = 10018)
	private String acountId;

	@KeyValuePairField(tag = 40)
	private String orderType;

	@KeyValuePairField(tag = 59)
	private String timeInForce;

	@KeyValuePairField(tag = 39)
	private String orderStatus;

	@KeyValuePairField(tag = 1)
	private String account;

	@KeyValuePairField(tag = 15)
	private String currency;

	@KeyValuePairField(tag = 14)
	private double cumQty;

	@KeyValuePairField(tag = 151)
	private double leavesQty;

	@KeyValuePairField(tag = 193)
	private String settlementDate;

	@KeyValuePairField(tag = 60)
	private String transactitionTime;

	public String getMarketOrderId() {
		return marketOrderId;
	}

	public void setMarketOrderId(String marketOrderId) {
		this.marketOrderId = marketOrderId;
	}

	public String getClientOrderId() {
		return clientOrderId;
	}

	public void setClientOrderId(String clientOrderId) {
		this.clientOrderId = clientOrderId;
	}

	public String getExecId() {
		return execId;
	}

	public void setExecId(String execId) {
		this.execId = execId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getCumQty() {
		return cumQty;
	}

	public void setCumQty(int cumQty) {
		this.cumQty = cumQty;
	}

	public double getAvgPx() {
		return avgPx;
	}

	public void setAvgPx(double avgPx) {
		this.avgPx = avgPx;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public String getTransactitionTime() {
		return transactitionTime;
	}

	public void setTransactitionTime(String transactitionTime) {
		this.transactitionTime = transactitionTime;
	}

	/*
	 * public List<BrokerExecutionReport> getBrokerExeReport() { return
	 * brokerExeReport; }
	 * 
	 * public void setBrokerExeReport(List<BrokerExecutionReport>
	 * brokerExeReport) { this.brokerExeReport = brokerExeReport; }
	 */
	public double getLastPX() {
		return lastPX;
	}

	public void setLastPX(double lastPX) {
		this.lastPX = lastPX;
	}

	public double getText() {
		return text;
	}

	public void setText(double text) {
		this.text = text;
	}

	public String getAcountId() {
		return acountId;
	}

	public void setAcountId(String acountId) {
		this.acountId = acountId;
	}

	public String getTimeInForce() {
		return timeInForce;
	}

	public void setTimeInForce(String timeInForce) {
		this.timeInForce = timeInForce;
	}

	public double getLeavesQty() {
		return leavesQty;
	}

	public void setLeavesQty(double leavesQty) {
		this.leavesQty = leavesQty;
	}

	public String getSettlementDate() {
		return settlementDate;
	}

	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}

	public void setCumQty(double cumQty) {
		this.cumQty = cumQty;
	}

	public String getOrderId() {
		return clientOrderId;
	}

	public int getQunantity() {
		return (int) cumQty;
	}

	public double getPrice() {
		return avgPx;
	}

	public Date getTradeDate() {
		return new Date();
	}

	public String getCurrencyPair() {
		return symbol;
	}

	public String getExchangeName() {
		return Constants.NORDA;
	}

	public String getOrderSide() {
		if ("1".equalsIgnoreCase(side))
			return "BUY";

		else if ("2".equalsIgnoreCase(side))
			return "SELL";
		else
			return "";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "TradeExecutionReport ["
				+ (id != null ? "id=" + id + ", " : "")
				+ (marketOrderId != null ? "marketOrderId=" + marketOrderId
						+ ", " : "")
				/*
				 * + (brokerExeReport != null ? "brokerExeReport=" +
				 * brokerExeReport + ", " : "")
				 */
				+ (clientOrderId != null ? "clientOrderId=" + clientOrderId
						+ ", " : "")
				+ (execId != null ? "execId=" + execId + ", " : "")
				+ (side != null ? "side=" + side + ", " : "")
				+ "avgPx="
				+ avgPx
				+ ", lastPX="
				+ lastPX
				+ ", "
				+ (symbol != null ? "symbol=" + symbol + ", " : "")
				+ (orderType != null ? "orderType=" + orderType + ", " : "")
				+ (orderStatus != null ? "orderStatus=" + orderStatus + ", "
						: "")
				+ (account != null ? "account=" + account + ", " : "")
				+ (currency != null ? "currency=" + currency + ", " : "")
				+ "cumQty="
				+ cumQty
				+ ", timeInForce="
				+ timeInForce
				+ ", "
				+ (transactitionTime != null ? "transactitionTime="
						+ transactitionTime : "") + "]";
	}

}
