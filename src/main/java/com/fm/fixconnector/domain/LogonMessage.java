package com.fm.fixconnector.domain;

import org.apache.camel.dataformat.bindy.annotation.KeyValuePairField;
import org.apache.camel.dataformat.bindy.annotation.Link;
import org.apache.camel.dataformat.bindy.annotation.Message;

@Message(pairSeparator = "\\u0001",keyValuePairSeparator = "=", type = "FIX", version = "4.4")
public class LogonMessage {

	@Link private Header header;
	
	@KeyValuePairField(tag = 52)
	private String sendingDateTime;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public String getSendingDateTime() {
		return sendingDateTime;
	}

	public void setSendingDateTime(String sendingDateTime) {
		this.sendingDateTime = sendingDateTime;
	}
	
}
