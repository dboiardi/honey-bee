package com.fm.fixconnector.domain;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Transient;

public class PriceUpdate {

	private String currencyPair;
	
	@Transient
	private long bidSize;
	@Transient
	private long askSize;
	
	private long bestbidSize;
	private long bestaskSize;
	
	// BidPrice 
	private double bestBidPrice;
	private double bestAskPrice;
	
	private String bestBidLP;
	private String bestASkLP;

	private Double spread;
	
	@Transient
	private DateTime bidpriceUpdateTime=DateTime.now();
	@Transient
	private DateTime askpriceUpdateTime=DateTime.now();
	
	@Transient
	private boolean updated;
	
	private DateTime dateTime=DateTime.now();
	
	public PriceUpdate(String currencyPair, long bidSize, long askSize,
			long bestbidSize, long bestaskSize, double bidPrice,
			double askPrice, String bestBidLP, String bestASkLP) {
		super();
		this.currencyPair = currencyPair;
		this.bidSize = bidSize;
		this.askSize = askSize;
		this.bestbidSize = bestbidSize;
		this.bestaskSize = bestaskSize;
		this.bestBidPrice = bidPrice;
		this.bestAskPrice = askPrice;
		this.bestBidLP = bestBidLP;
		this.bestASkLP = bestASkLP;
		this.spread = (bestAskPrice==0||bestBidPrice == 0)?null:bestAskPrice-bestBidPrice; 
	}

	public long getBestbidSize() {
		return bestbidSize;
	}
	
	public long getBestaskSize() {
		return bestaskSize;
	}

	public String getCurrencyPair() {
		return currencyPair;
	}


	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair;
	}


	public Long getBidSize() {
		return bidSize;
	}

	public void setBidSize(Long bidSize) {
		this.bidSize = bidSize;
	}

	public Long getAskSize() {
		return askSize;
	}

	public void setAskSize(Long askSize) {
		this.askSize = askSize;
	}

	public void setBestBidLP(String bestBidLP) {
		this.bestBidLP = bestBidLP;
	}

	public void setBestASkLP(String bestASkLP) {
		this.bestASkLP = bestASkLP;
	}
	

	public String getBestBidLP() {
		return bestBidLP;
	}

	public String getBestASkLP() {
		return bestASkLP;
	}

	public void setBidSize(long bidSize) {
		this.bidSize = bidSize;
	}

	public void setAskSize(long askSize) {
		this.askSize = askSize;
	}

	public void setBestbidSize(long bestbidSize) {
		this.bestbidSize = bestbidSize;
	}

	public void setBestaskSize(long bestaskSize) {
		this.bestaskSize = bestaskSize;
	}
	public DateTime getBidpriceUpdateTime() {
		return bidpriceUpdateTime;
	}

	public void setBidpriceUpdateTime(DateTime bidpriceUpdateTime) {
		this.bidpriceUpdateTime = bidpriceUpdateTime;
	}

	public DateTime getAskpriceUpdateTime() {
		return askpriceUpdateTime;
	}

	public void setAskpriceUpdateTime(DateTime askpriceUpdateTime) {
		this.askpriceUpdateTime = askpriceUpdateTime;
	}

	public boolean isUpdated() {
		return this.updated;
	}

	public void setUpdated(boolean updated) {
		this.updated = updated;
		dateTime=DateTime.now();
	}

	
	public DateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(DateTime dateTime) {
		this.dateTime = dateTime;
	}

	public double getBestBidPrice() {
		return bestBidPrice;
	}

	public void setBestBidPrice(double bestBidPrice) {
		this.bestBidPrice = bestBidPrice;
		this.spread = (bestAskPrice==0||bestBidPrice == 0)?null:bestAskPrice-bestBidPrice;
	}

	public double getBestAskPrice() {
		return bestAskPrice;
	}

	public void setBestAskPrice(double bestAskPrice) {
		
		this.bestAskPrice = bestAskPrice;
		this.spread = (bestAskPrice==0||bestBidPrice == 0)?null:bestAskPrice-bestBidPrice;
	}

	public double getSpread() {
		return spread!=null?spread:0;
	}

	public void setSpread(double spread) {
		this.spread = spread;
	}
	
	

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
	
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return "PriceUpdate ["
				+ (currencyPair != null ? "currencyPair=" + currencyPair + ", "
						: "")
				+ "bidSize="
				+ bidSize
				+ ", askSize="
				+ askSize
				+ ", bestbidSize="
				+ bestbidSize
				+ ", bestaskSize="
				+ bestaskSize
				+ ", bestBidPrice="
				+ bestBidPrice
				+ ", bestAskPrice="
				+ bestAskPrice
				+ ", "
				+ (bestBidLP != null ? "bestBidLP=" + bestBidLP + ", " : "")
				+ (bestASkLP != null ? "bestASkLP=" + bestASkLP + ", " : "")
				+ "spread="
				+ spread
				+ ", "
				+ (bidpriceUpdateTime != null ? "bidpriceUpdateTime="
						+ bidpriceUpdateTime + ", " : "")
				+ (askpriceUpdateTime != null ? "askpriceUpdateTime="
						+ askpriceUpdateTime + ", " : "") + "updated="
				+ this.updated + ", "
				+ (dateTime != null ? "dateTime=" + dateTime : "") + "]";
	}

}
