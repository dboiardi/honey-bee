package com.fm.fixconnector.service;

import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.util.StringUtils;

import com.fm.fixconnector.database.GenericDAO;
import com.fm.fixconnector.domain.*;
import com.fm.fixconnector.exception.NotValidStrategyException;
import com.fm.fixconnector.listner.StrategyListner;
public class StrategyService {

	static final Logger logger = Logger.getLogger(StrategyService.class);
	private GenericDAO genericDAO;
	private InstrumentService instrumentService;
	private StrategyListner strategyListner;
	private ObjectMapper objectMapper=new ObjectMapper();
	
	public void saveStrategy(String name){
		
		Strategy strategy=new Strategy(name, true);
		genericDAO.saveObject(strategy);
		logger.info("Strategy saved successfully "+strategy);
		
	}
	
	public String getStratgey(String name){
		List<Strategy> stratgies= (List<Strategy>) genericDAO.getObjectListForCLass(Strategy.class);
		Strategy retStrategy = null;
		for(Strategy strategy:stratgies){
			if(org.apache.commons.lang.StringUtils.containsIgnoreCase(strategy.getName(), name)){
				retStrategy = strategy;
				break;
			}
		}
		if(retStrategy!=null){
			try {
				return objectMapper.writeValueAsString(retStrategy);
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			return "";
		
	}
	
	public String getStrategies() throws JsonGenerationException, JsonMappingException, IOException{
		
		List<Strategy> strategies=(List<Strategy>) genericDAO.getObjectListForCLass(Strategy.class);
		/*List<String> stratNames=new ArrayList<String>();
		for(Strategy strategy:strategies){
			stratNames.add(strategy.getName());
		}*/
		
		return objectMapper.writeValueAsString(strategies);
		
	}
	
	public String getStrategyContent(Exchange exchange) throws JsonGenerationException, JsonMappingException, IOException, NotValidStrategyException{
	
	String stratcontent = exchange.getIn().getBody(String.class);
		
		if (StringUtils.hasText(stratcontent)) {
			
			String paramaMap[]=stratcontent.split("=");
			String name=paramaMap[1];
			Strategy strategy= genericDAO.getStrategy(name);
			return objectMapper.writeValueAsString(strategy);
		}
		else{
			throw new NotValidStrategyException("No name found");
		}
	}
	
	public String process(Exchange exchange) throws NotValidStrategyException {
		
		
		String stratcontent = exchange.getIn().getBody(String.class);
		
		if (StringUtils.hasText(stratcontent)) {
		
			String decodedurl = URLDecoder.decode(exchange.getIn().getBody(String.class));
			String paramaMap[]=decodedurl.split("&");
			try {
				StrategyCurrencyPair[] currPair=new ObjectMapper().readValue(decodedurl.split("=")[1], StrategyCurrencyPair[].class);
				if(!StringUtils.hasText(currPair[0].getStratName())){
					throw new NotValidStrategyException("hhh");
				}
				
				Strategy strategy=genericDAO.getStrategy(currPair[0].getStratName());
				
				if(strategy==null){
					strategy=new Strategy(currPair[0].getStratName(), true);	
				}else{
					
					logger.info("updated in existing strategy "+currPair[0].getStratName());
				
					for (StrategyCurrencyPair inst : strategy.getSecuritys()) {
						TradeableSecurity security=genericDAO.getTradeEableSecurityforInstrument(inst.getCurrencyPair());
						if ( security!= null) {
							genericDAO.saveObject(security);
						} else {
							logger.error("We don't have instrument" + inst);
						}
					}
					
				}
				
				strategy.setSecuritys(Arrays.asList(currPair));
				strategy.setAmount(strategy.getSecuritys().get(0).getAmount());
				strategy.setReportingCurrencyPair(currPair[0].getReportingCurrency());
				strategy.setReportingCurrencypriceType(currPair[0].getReportingPriceType());
				strategy.setReportingCurrency(currPair[0].getReportingCurr());
				
				if("true".equalsIgnoreCase(currPair[0].getIsActive())){
					
				
				for (StrategyCurrencyPair inst : strategy.getSecuritys()) {
					TradeableSecurity security=genericDAO.getTradeEableSecurityforInstrument(inst.getCurrencyPair());
					if ( security!= null) {
						instrumentService.subscribeForPrice(inst.getCurrencyPair());
						security.setActive(true);
						genericDAO.saveObject(security);
					} else {
						logger.error("We don't have instrument" + inst);
					}
				}
				
				instrumentService.subscribeForPrice(strategy.getReportingCurrencyPair());
				strategyListner.addStratgey(strategy);
				}else{
					strategy.setActive(false);
					strategyListner.stopPreviousThread(strategy);
					logger.info("strategy has been stooped "+strategy.getName());
				}
					genericDAO.saveObject(strategy);
					logger.info("Saved Strategy " + strategy);
			} catch (Exception e) {
				e.printStackTrace();
				throw new NotValidStrategyException("hhh");
			}
			return "Success";
			
			}else{
				throw new NotValidStrategyException("hhh");
			}
	}
	
	public boolean deleteStratgey(String name){
		return genericDAO.deleteStrategy(name)>0?true:false;
	}
	
	public List<String> getInstrument(String name) throws NotValidStrategyException{
		List<String> result=new ArrayList<String>();
		String[] cuurency=name.split("_");
		if(cuurency!=null && cuurency.length>=2){
			for(int i=0;i<cuurency.length-1;i++){
				result.add(cuurency[i]+"/"+cuurency[i+1]);
			}
			logger.debug(name +" startegy have "+result.size()+" security pairs"+result);
			
		}else{
			logger.error("Unable to parse currency "+name);
			throw new NotValidStrategyException(name+"is not valid");
		}
		return result;
	}

	public GenericDAO getGenericDAO() {
		return genericDAO;
	}

	public void setGenericDAO(GenericDAO genericDAO) {
		this.genericDAO = genericDAO;
	}

	public InstrumentService getInstrumentService() {
		return instrumentService;
	}

	public void setInstrumentService(InstrumentService instrumentService) {
		this.instrumentService = instrumentService;
	}

	public StrategyListner getStrategyListner() {
		return strategyListner;
	}

	public void setStrategyListner(StrategyListner strategyListner) {
		this.strategyListner = strategyListner;
	}
	
}
