package com.fm.fixconnector.service;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.camel.Exchange;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.util.StringUtils;

import com.fm.fixconnector.repo.StrategyUpdateRepo;

public class TradeMonitorService {

	private static String STRAT_NAME="stratName";
	private static String CURRENCY_PAIR="currencyPair";
	private StrategyUpdateRepo strategyUpdateRepo;
	
	
	private static ConcurrentHashMap<String, Boolean> tradeMonitorEnabled=new ConcurrentHashMap<>();
	
	public static Boolean getStrategyStatus(String strategyName){
		return tradeMonitorEnabled.get(strategyName);
		
	}
	
	public String getStrategyUpdate(Exchange exchange){
		String strategyName=(String) exchange.getIn().getHeader(STRAT_NAME);
		if(StringUtils.hasText(strategyName)){
			try {
			return	strategyUpdateRepo.getByName(strategyName);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
		
	}

	public StrategyUpdateRepo getStrategyUpdateRepo() {
		return strategyUpdateRepo;
	}

	public void setStrategyUpdateRepo(StrategyUpdateRepo strategyUpdateRepo) {
		this.strategyUpdateRepo = strategyUpdateRepo;
	}
	
	
}
