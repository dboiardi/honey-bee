package com.fm.fixconnector.service;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.fm.fixconnector.FixMessageController;
import com.fm.fixconnector.database.GenericDAO;
import com.fm.fixconnector.domain.Strategy;
import com.fm.fixconnector.domain.StrategyCurrencyPair;
import com.fm.fixconnector.domain.TradeableSecurity;


public class InstrumentService {

	static final Logger logger = Logger.getLogger(InstrumentService.class);
	private FixMessageController pricefixController;
	private HashMap<Integer,String> priceReqMap;
	private int priceField;
	private GenericDAO genericDAO;
	private HashMap<String ,Double> subscribedMap=new HashMap<String, Double>();
	ObjectMapper mapper = new ObjectMapper();
	
	public void reset(){
		subscribedMap.clear();
	}
	
	public void loadOnStartUp(){

		List<Strategy> tradeableSecurites = (List<Strategy>) genericDAO.getObjectListForCLass(Strategy.class);
		int count = 0;
		for (Strategy strategy : tradeableSecurites) {
			if (strategy.isActive()) {
				for (StrategyCurrencyPair currencyPair : strategy.getSecuritys()) {

					subscribeForPrice(currencyPair.getCurrencyPair());
					count++;
				}
				subscribeForPrice(strategy.getReportingCurrencyPair());
			}
		}
		logger.info("Total Securities " + tradeableSecurites.size()+ " Susbcribed Security " + count);

	}
	
	public void subscribeForPrice(String instrument){
		
		if(!subscribedMap.containsKey(instrument)){
		
			priceReqMap.put(priceField, instrument);
			pricefixController.sendPriceRequestMessage(priceReqMap);
			subscribedMap.put(instrument, (double) 0);
			
		}else{
			logger.info("Not Subscribing since already subscribed "+instrument);
		}
		
	}
	
	public void unSubscribeForPrice(String instrument){
		
			priceReqMap.put(priceField, instrument);
			priceReqMap.put(263, "2");
			pricefixController.sendPriceRequestMessage(priceReqMap);
			subscribedMap.remove(instrument);
			
	}
	
	
	public String getTradeableSecurity(){
		
		List<TradeableSecurity> securities=(List<TradeableSecurity>) genericDAO.getObjectListForCLass(TradeableSecurity.class);
		Collections.sort(securities, new Comparator<TradeableSecurity>() {

			@Override
			public int compare(TradeableSecurity o1, TradeableSecurity o2) {
				return o1.getSymbol().compareTo(o2.getSymbol());
			}
		});
		String returnStr="";
		try {
			returnStr = mapper.writeValueAsString(securities);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnStr;
	}

	public FixMessageController getPricefixController() {
		return pricefixController;
	}

	public void setPricefixController(FixMessageController pricefixController) {
		this.pricefixController = pricefixController;
	}

	public HashMap<Integer, String> getPriceReqMap() {
		return priceReqMap;
	}

	public void setPriceReqMap(HashMap<Integer, String> priceReqMap) {
		this.priceReqMap = priceReqMap;
	}

	public int getPriceField() {
		return priceField;
	}

	public void setPriceField(int priceField) {
		this.priceField = priceField;
	}

	public GenericDAO getGenericDAO() {
		return genericDAO;
	}

	public void setGenericDAO(GenericDAO genericDAO) {
		this.genericDAO = genericDAO;
	}
	
}
