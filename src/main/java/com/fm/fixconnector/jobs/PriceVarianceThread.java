package com.fm.fixconnector.jobs;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.util.MathUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.fm.fixconnector.analytics.QuoteFrequency;
import com.fm.fixconnector.analytics.StatisticsAnalytics;
import com.fm.fixconnector.database.GenericDAO;
import com.fm.fixconnector.domain.PriceType;
import com.fm.fixconnector.domain.PriceUpdate;
import com.fm.fixconnector.nordea.MarketData;
import com.fm.fixconnector.repo.PriceRepo;
import com.fm.fixconnector.util.CustomThreadName;

public class PriceVarianceThread extends Thread{
	
	GenericDAO genericDAO;
	PriceRepo priceRepo;
	private int second=1;
	private DateTime time=DateTime.now().withMillisOfSecond(0);
	static final Logger logger = Logger.getLogger(PriceVarianceThread.class);
	private static ScheduledExecutorService executorService;
	DecimalFormat df = new DecimalFormat("#.#####");
	
	
	public void init(){
		executorService=Executors.newScheduledThreadPool(1,new CustomThreadName("PriceVarianceThread"));
		executorService.scheduleAtFixedRate(this, 0, 1000, TimeUnit.MILLISECONDS);
		logger.info("PriceVarianceThread started");
	}
	
	public void run(){
		try{
		logger.debug("Price Variance Thread running "+time);	
		time=time.plusSeconds(second);
		ConcurrentHashMap<String, PriceUpdate> lastUpdate =  priceRepo.getRecentPriceUpdateMap();
		
		for (Entry<String, PriceUpdate> entry : lastUpdate.entrySet()) {
		
			calculateStatistic(entry,PriceType.BID,time);
			calculateStatistic(entry,PriceType.ASK,time);
			calculateStatisticOnBestPrice(entry,time);
			
		}
		}catch(Exception exception){
			logger.error("Error in PriceVarianceThread "+ExceptionUtils.getFullStackTrace(exception));
		}
	}
	
	public void calculateStatisticOnBestPrice(Entry<String, PriceUpdate> entry,DateTime dateTime) {
		try{
		List<PriceUpdate> values= genericDAO.getPriceUpdateForLastNSecond(entry.getKey(), second, dateTime);
		double[] bestBid=new double[values.size()];
		double[] bestAsk=new double[values.size()];
		int bidK=0;
		int askK=0;
		for(int i=0;i<values.size()-1;i++){
			if(values.get(i+1).getBestBidPrice()!=0 && values.get(i).getBestBidPrice()!=0){
				bestBid[bidK]=values.get(i+1).getBestBidPrice()-values.get(i).getBestBidPrice();
				bidK++;
			}
			
			if(values.get(i+1).getBestAskPrice()!=0 && values.get(i).getBestAskPrice()!=0){
				bestAsk[askK]=values.get(i+1).getBestAskPrice()-values.get(i).getBestAskPrice();
				askK++;
			}
			
		}
		
		double variance=StatUtils.variance(bestBid);
		double deviation=Math.sqrt(variance);
		double sigma_2=2*deviation;
		double sigma_3=3*deviation;
		
		StatisticsAnalytics analyticsBid=new StatisticsAnalytics(entry.getKey(), PriceType.BID, variance, deviation, deviation, sigma_2, sigma_3, dateTime,"BESTPRICEFORALLLP");
		genericDAO.saveObject(analyticsBid);
	
		variance=StatUtils.variance(bestAsk);
		deviation=Math.sqrt(variance);
		sigma_2=2*deviation;
		sigma_3=3*deviation;
		
		StatisticsAnalytics analyticsAsk=new StatisticsAnalytics(entry.getKey(), PriceType.BID, variance, deviation, deviation, sigma_2, sigma_3, dateTime,"BESTPRICEFORALLLP");
		genericDAO.saveObject(analyticsAsk);
		}catch(Exception exception){
			logger.error("Error in variance for PriceUpdated "+ExceptionUtils.getFullStackTrace(exception));
		}
	}

	public void calculateStatistic(Entry<String, PriceUpdate> entry,PriceType priceType,DateTime dateTime) {
		
		List<MarketData> values= genericDAO.getMarketDataForLastNSecond(entry.getKey(), second, dateTime,priceType);
		
		// This is map is used to store LP - lastVlaue-firstValue/firstValue
		Map<String, List<Double>> lpPriceDiff=new HashMap<>();
		
		// This map is used to store Temporary Last Value;
		Map<String, Double> lastPriceForLP=new HashMap<>();
		
		for(MarketData mdata:values){
			Double lastPrice=lastPriceForLP.get(mdata.getMdEntryOriginator());
			if(lastPrice!=null){
				// If last Price exist it means we need to add
			List<Double> LpPriceDiffForLp = lpPriceDiff.get(mdata.getMdEntryOriginator());
			
			double diff=Math.abs(mdata.getPrice()-lastPrice/lastPrice);
			
			if(LpPriceDiffForLp==null){
				LpPriceDiffForLp=new ArrayList<>();
				lpPriceDiff.put(mdata.getMdEntryOriginator(), LpPriceDiffForLp);
			}
			
			LpPriceDiffForLp.add(diff);	
			
			}else{
				lastPriceForLP.put(mdata.getMdEntryOriginator(),mdata.getPrice());
			}
		
		}
		
		
		for(Entry<String, List<Double>> lpEntrySet:lpPriceDiff.entrySet()){
		
			 Double[] diffValues = lpEntrySet.getValue().toArray(new Double[lpEntrySet.getValue().size()]);
		      
		
		     double[] primitivesDiffValues = ArrayUtils.toPrimitive(diffValues);

			double variance=StatUtils.variance(primitivesDiffValues);
			double deviation=Math.sqrt(variance);
			double sigma_2=2*deviation;
			double sigma_3=3*deviation;
			
			StatisticsAnalytics analytics=new StatisticsAnalytics(entry.getKey(), priceType, variance, deviation, deviation, sigma_2, sigma_3, dateTime,lpEntrySet.getKey());
			genericDAO.saveObject(analytics);
			
		}
		
	}

	public GenericDAO getGenericDAO() {
		return genericDAO;
	}

	public void setGenericDAO(GenericDAO genericDAO) {
		this.genericDAO = genericDAO;
	}

	public PriceRepo getPriceRepo() {
		return priceRepo;
	}

	public void setPriceRepo(PriceRepo priceRepo) {
		this.priceRepo = priceRepo;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}

	public DateTime getTime() {
		return time;
	}

	public void setTime(DateTime time) {
		this.time = time;
	}
	
	
}
