package com.fm.fixconnector.jobs;

import com.fm.fixconnector.repo.PriceRepo;

public class LastSecondTickClearnerThread  extends Thread{

	PriceRepo priceRepo;
	
	public LastSecondTickClearnerThread(PriceRepo priceRepo) {
		super();
		this.priceRepo = priceRepo;
	}


	public void run(){
		priceRepo.clearTicksInLastSecond();	
	}
	
}
