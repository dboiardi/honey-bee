package com.fm.fixconnector.jobs;

import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.fm.fixconnector.analytics.QuoteFrequency;
import com.fm.fixconnector.database.GenericDAO;
import com.fm.fixconnector.domain.PriceUpdate;
import com.fm.fixconnector.repo.PriceRepo;

// Presently this class is used to update frequency in db
public class PriceUpdateMonitorJob extends Thread {

	private PriceRepo priceRepo;
	private GenericDAO dao;
	private long interval = 250;
	private long timeInterval;
	private String timeUnit;

	static final Logger logger = Logger.getLogger(PriceUpdateMonitorJob.class);

	public PriceUpdateMonitorJob(PriceRepo priceRepo, GenericDAO genericDAO,long timeInterval,String timeUnit) {
		super();
		this.priceRepo = priceRepo;
		this.dao=genericDAO;
		this.timeInterval=timeInterval;
		this.timeUnit=timeUnit;
	}

	public void run() {
		try {

			DateTime currentTime = DateTime.now();

			ConcurrentHashMap<String, PriceUpdate> lastUpdate =  priceRepo.getRecentPriceUpdateMap();

			for (Entry<String, PriceUpdate> entry : lastUpdate.entrySet()) {
			
				dao.saveObject(new QuoteFrequency(entry.getKey(), priceRepo.getUpdateInLastSecond(entry.getKey()),DateTime.now(),timeInterval,timeUnit));
				/*if (currentTime.getMillis() - entry.getValue().getBidpriceUpdateTime().getMillis() > interval) {
					priceRepo.removePrice(entry.getKey());
					logger.debug("Removing price for " + entry.getKey()+ " Becuase no updated was there CurrentTime "+currentTime+" lastUpdate "+entry.getValue());
				}*/
			}
		} catch (Exception exception) {
			logger.error("Error occured in priceUpdateMonitor Thread "+ exception.getLocalizedMessage());
			exception.printStackTrace();
		}
	}

}
