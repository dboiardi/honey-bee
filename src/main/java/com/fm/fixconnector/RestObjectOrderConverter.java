package com.fm.fixconnector;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;

public class RestObjectOrderConverter{

	public Map<Integer,String> process(Exchange exchange) throws Exception {
		
		Map<Integer,String> retMap=new HashMap<Integer, String>();
			int orderSize=Integer.parseInt(exchange.getIn().getHeader("size").toString());
			String currency= exchange.getIn().getHeader("currency").toString().replace("-", "/");
			int orderSide=Integer.parseInt(exchange.getIn().getHeader("side").toString());
			
			retMap.put(55, currency);
			retMap.put(54, String.valueOf(orderSide));
			retMap.put(38, String.valueOf(orderSize));
			retMap.put(40, String.valueOf(orderSize));
			
		return retMap;
		
	}

}
