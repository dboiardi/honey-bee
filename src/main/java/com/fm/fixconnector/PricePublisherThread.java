package com.fm.fixconnector;

import java.util.concurrent.ArrayBlockingQueue;

import org.apache.log4j.Logger;

import com.fm.fixconnector.nordea.MarketData;
import com.fm.fixconnector.repo.PriceRepo;

public class PricePublisherThread  extends Thread{

	static final Logger logger = Logger.getLogger(QueueMessageListner.class);
	
	private ArrayBlockingQueue<MarketData> queue;
	
	public PricePublisherThread(ArrayBlockingQueue<MarketData> queue,PriceRepo priceRepo) {
		this.queue=queue;
	}
	
	public void run(){
		MarketData data;
		try {
			data = queue.take();
			printLatency(data);
			while(data!=null){
				
				data=queue.take();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
	}

	private void printLatency(MarketData data) {
		
	}
	
}
