package com.fm.fixconnector;



import java.util.Date;
import java.util.Map;
import java.util.Random;

import com.fm.fixconnector.domain.AbstractMarketData;
import com.fm.fixconnector.seb.QuoteData;
import com.fm.fixconnector.util.Constants;

import quickfix.DefaultMessageFactory;
import quickfix.Group;
import quickfix.Message;
import quickfix.field.MsgType;
import quickfix.field.NoMDEntryTypes;


public class MessageRepo {
	
	private String quoteId;
	
	protected String messageVersion="FIX.4.4";
	String senderCompId;
	String targetCompId;
	String userName;
	String password;
	
	protected quickfix.MessageFactory factory=new DefaultMessageFactory();
	
	public  void setQuoteId(QuoteData md) {
			this.quoteId = md.getQuoteId();
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getQuoteId() {
		return quoteId;
	}
	
	public String getMessageVersion() {
		return messageVersion;
	}
	public void setMessageVersion(String messageVersion) {
		this.messageVersion = messageVersion;
	}
	public String getSenderCompId() {
		return senderCompId;
	}
	public void setSenderCompId(String senderCompId) {
		this.senderCompId = senderCompId;
	}
	public String getTargetCompId() {
		return targetCompId;
	}
	public void setTargetCompId(String targetCompId) {
		this.targetCompId = targetCompId;
	}
	
	public  Message getPriceStreamMessage(Map<Integer,String> valueMap){
	
		Message message=factory.create(messageVersion, MsgType.MARKET_DATA_REQUEST);
		
		message.getHeader().setString(49, senderCompId);
		message.getHeader().setString(56, targetCompId);
		
		message.setInt(263, Integer.parseInt(valueMap.get(263)));
		message.setInt(264, Integer.parseInt(valueMap.get(264)));
		message.setInt(265, Integer.parseInt(valueMap.get(265)));
		
		message.setString(262,String.valueOf(valueMap.get(55).replace("/", "_")+"#"+getRandomNo()));	
		int order[] ={269};
		Group group=new Group(267, 269,order);
		group.setInt(269,  Integer.parseInt(valueMap.get(269).split(",")[0]));
		group.setInt(269,  Integer.parseInt(valueMap.get(269).split(",")[1]));
		message.addGroup(group);
		
		Group group1=new Group(146,55);
		group1.setString(55, valueMap.get(55));
		message.addGroup(group1);
		
		return message;
	}
	
	public  Message getSecurityListMessage(Map<Integer,String> valueMap){
		
		Message message=factory.create(messageVersion, MsgType.SECURITY_LIST_REQUEST);
		
		message.getHeader().setString(49, senderCompId);
		message.getHeader().setString(56, targetCompId);
		
		message.setInt(320, getRandomNo());
		message.setInt(559, Integer.parseInt(valueMap.get(559)));
		
		return message;
	}
	
	public  Message getOrderMessage(Map<Integer,String> valueMap){
		
		Message message=factory.create(messageVersion, MsgType.ORDER_SINGLE);
		
		int orderSide=0;
		
		
		message.getHeader().setString(49, senderCompId);
		message.getHeader().setString(56, targetCompId);
		
		message.setString(11, String.valueOf(getRandomNo()));
		message.setString(55,valueMap.get(55));
		message.setString(54, valueMap.get(54));
		message.setUtcTimeStamp(60, new Date());
		message.setString(38, valueMap.get(38));
		message.setString(40, String.valueOf(1));
		message.setInt(59, 1);
		
		return message;
	}
	
	public Message updateLogonMessage(Message message){
		
		message.setString(553,getUserName());
		message.setString(554,getPassword());
		
		return message;
	}
	
	public Message updateLogonMessageForHostpot(Message message){
		
		updateLogonMessage(message);
		
		message.setString(98,"0");
		message.setString(108,"30");
		message.removeField(141);
		return message;
	}
	
	public int getRandomNo(){
		Random r=new Random();
		int no= r.nextInt();
		if(no<0){
			no=no*-1;	
		}
		return no;
	}
	

	
}
