package com.fm.fixconnector.exception;

public class NotValidStrategyException extends Exception {

	public NotValidStrategyException(String string) {
		super(string);
	}

}
