package com.fm.fixconnector.converter;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.fm.fixconnector.domain.TradeExecutionReport;

public class TradeExecutionToJsonConverter {

	private ObjectMapper objectMapper=new ObjectMapper();
	
	public String convertToJson(TradeExecutionReport report){
		try {
			return objectMapper.writeValueAsString(report);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
		
	}
}
