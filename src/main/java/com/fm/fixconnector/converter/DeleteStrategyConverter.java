package com.fm.fixconnector.converter;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;

public class DeleteStrategyConverter {

	public String convert(Exchange exchange) throws Exception {
	
		return exchange.getIn().getHeader("name").toString();
	}

}
