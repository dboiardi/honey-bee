package com.fm.fixconnector.converter;

import java.io.InputStream;

import org.apache.camel.Exchange;
import org.apache.camel.dataformat.bindy.kvp.BindyKeyValuePairDataFormat;
import org.joda.time.DateTime;

import com.fm.fixconnector.nordea.MarketData;
import com.fm.fixconnector.util.DateUtil;

public class MarketDataConverter extends BindyKeyValuePairDataFormat{

	public MarketDataConverter(Class<?> type) {
        super(type);
    }
	
	public Object unmarshal(Exchange exchange, InputStream inputStream) throws Exception {
		Object obj=super.unmarshal(exchange, inputStream);
		if(obj instanceof MarketData){
			MarketData marketData=(MarketData)obj;
			marketData.setClientTime(DateTime.now());
			marketData.setServerTime(new DateTime(DateUtil.getFormatedSendingTime(marketData.getSendingTime())));
			marketData.setCurrency(marketData.getCurrency());
			
			return marketData;
		}
		return obj;
	}
}
