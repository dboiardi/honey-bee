package com.fm.fixconnector.algo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.fm.fixconnector.domain.AbstractMarketData;
import com.fm.fixconnector.domain.AbstractTradeCapture;
import com.fm.fixconnector.util.Constants;
import com.fm.fixconnector.util.RandomNumberGenerator;

public class TradeReconcilation {

	ConcurrentHashMap<String,Object> activeTrades=new ConcurrentHashMap<String,Object>();
	Queue<AbstractMarketData> pendingOrder;
	static final Logger logger = Logger.getLogger(TradeReconcilation.class);
	
	public Queue<AbstractMarketData> getPendingOrder() {
		return pendingOrder;
	}
	public void setPendingOrder(Queue<AbstractMarketData> pendingOrder) {
		this.pendingOrder = pendingOrder;
	}
	public void beforePlacingTrade(Map map,Object object){
	
		(new RandomNumberGenerator()).assignNo(map);
	    logger.info("Putting message on map for "+map.get(Constants.ID)+" "+object);
		if(object!=null){
			activeTrades.put((String) map.get(Constants.ID), object);	
		}
	    
	}
	public void OnComepleteTrade(AbstractTradeCapture tradeCapture){
		
		activeTrades.remove(tradeCapture.getOrderId());
		logger.info("Removing "+tradeCapture.getOrderId()+" from map since it executed successfully");
	}
	public void OnPartiallyComepleteTrade(AbstractTradeCapture tradeCapture){
		logger.info("OnPartiallyComepleteTrade "+tradeCapture.getOrderId()+" from map ");
		AbstractMarketData marketData=(AbstractMarketData)activeTrades.get(tradeCapture.getOrderId());
		int qunatity=marketData.getQuantity()-tradeCapture.getQunantity();
		marketData.setQuantity(qunatity);
		logger.info("added On queue "+tradeCapture.getOrderId()+" from map qunatity"+qunatity+"Market Data"+marketData);
		pendingOrder.add(marketData);
		activeTrades.remove(tradeCapture.getOrderId());
		
	}
	public void OnFailedTrade(AbstractTradeCapture tradeCapture){
		logger.info("OnFailedTrade "+tradeCapture.getOrderId()+" from map ");
		AbstractMarketData marketData=(AbstractMarketData)activeTrades.get(tradeCapture.getOrderId());
		logger.info("Market data from Map "+marketData+" from map ");
		int qunatity=marketData.getQuantity();
		marketData.setQuantity(qunatity);
		logger.info("added On queue "+tradeCapture.getOrderId()+" from map qunatity"+qunatity+"Market Data"+marketData);
		pendingOrder.add(marketData);
		activeTrades.remove(tradeCapture.getOrderId());
	}
}
