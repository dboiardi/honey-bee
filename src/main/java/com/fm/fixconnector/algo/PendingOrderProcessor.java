package com.fm.fixconnector.algo;

import java.util.ArrayList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import com.fm.fixconnector.MessageController;
import com.fm.fixconnector.domain.AbstractMarketData;

public class PendingOrderProcessor extends Thread {

	static final Logger logger = Logger.getLogger(PendingOrderProcessor.class);
	private BlockingQueue<AbstractMarketData> pendingOrder;
	private Map<String,PriceMatcher> priceMatchers;
	private AbstractMarketData marketData;
	private MessageController messageController;
	
	
	public BlockingQueue<AbstractMarketData> getPendingOrder() {
		return pendingOrder;
	}
	public void setPendingOrder(BlockingQueue<AbstractMarketData> pendingOrder) {
		this.pendingOrder = pendingOrder;
	}
	public Map<String, PriceMatcher> getPriceMatchers() {
		return priceMatchers;
	}
	public void setPriceMatchers(Map<String, PriceMatcher> priceMatchers) {
		this.priceMatchers = priceMatchers;
	}
	public void init(){
		//there need to placed order with live price
		this.start();
		logger.info("Pending Order Processor Thread Started");
	}
	public void run(){
		
		marketData = getMarketData();
		while(marketData!=null){
			PriceMatcher priceMatcher=priceMatchers.get(marketData.getSymbol());
			ArrayList<Object> obj=null;
			do{
				obj=priceMatcher.getPriceMessageController(marketData);
				if(!(obj!=null && obj.size()==2)){
					logger.error("Price didn't get upated so waiting for price update .."+marketData );
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block6
						e.printStackTrace();
					}
				}
				
			}while(!(obj!=null && obj.size()==2));
			messageController=(MessageController)obj.get(0);
			AbstractMarketData	marketDataNew=(AbstractMarketData) obj.get(1);
			if(messageController!=null){
				logger.error("placing order from Pending order Processor on message controller"+marketData+ " "+messageController);
				messageController.sendMessage(marketDataNew);	
			}else{
				logger.error("Unable to find message controller to place pending order");
			}
			
			marketData=getMarketData();
		}
	}

	private AbstractMarketData getMarketData() {
		try {
			marketData = pendingOrder.take();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return marketData;
	}
}
