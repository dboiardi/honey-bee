package com.fm.fixconnector.algo;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.fm.fixconnector.MessageController;
import com.fm.fixconnector.domain.AbstractMarketData;
import com.fm.fixconnector.domain.ApplicationcOrder;
import com.fm.fixconnector.util.CommanFunction;
import com.fm.fixconnector.util.Constants;

@ManagedResource
public class OrderGateway {

	static final Logger logger = Logger.getLogger(OrderGateway.class);
	
	

	public static int noOfMaxTrade = 10;
	private static int noOfTradeDoneforToday = 0;

	private  boolean waitingConfirmationforSource1 = false;
	private boolean waitingConfirmationforSource2 = false;
	private Date lastTradedTime;

	private static  ScheduledExecutorService executor = null;

	static {
		logger.info("On loading of class Starting reset Thread....");
		executor = Executors.newScheduledThreadPool(2);
		executor.scheduleAtFixedRate(new Runnable() {

			public void run() {
				logger.info("Resetiing Trade Done ...");
				noOfTradeDoneforToday = 0;
			}
		}, getHoursUntilTarget(1), 24, TimeUnit.HOURS);
		
		executor.scheduleAtFixedRate(new Runnable() {

			public void run() {
				logger.info("Stopping Trading before market close");
				noOfTradeDoneforToday =noOfMaxTrade+1;
			}
		}, getHoursUntilTarget(22), 24, TimeUnit.HOURS);

	}

	/*public static void main(String args[]){
	System.out.println("Time diff "+getHoursUntilTarget(22));	
	}*/

	public synchronized void placeOrder(MessageController sourceFirst,
			MessageController sourceSecond, AbstractMarketData buymarketData,
			AbstractMarketData sellMarketData) {
		if (noOfMaxTrade >= noOfTradeDoneforToday
				&& !waitingConfirmationforSource1
				&& !waitingConfirmationforSource2 && (lastTradedTime==null || CommanFunction.getDateDiffFromNow(lastTradedTime, TimeUnit.SECONDS)>=20 )) {
			logger.info("buymarketData "+buymarketData+" sellMarketData "+sellMarketData);
			sourceFirst.sendMessage(buymarketData);
			sourceSecond.sendMessage(sellMarketData);
			waitingConfirmationforSource1 = true;
			waitingConfirmationforSource2 = true;
			lastTradedTime=new Date();
			noOfTradeDoneforToday++;
			logger.info("No Of Order Placed " + noOfTradeDoneforToday);
			
		} else {
			if (waitingConfirmationforSource1 || waitingConfirmationforSource2) {
				logger.info("waiting for Order Confirmation from source1 "
						+ waitingConfirmationforSource1 + " Source2 "
						+ waitingConfirmationforSource2);
			}
			if(noOfMaxTrade < noOfTradeDoneforToday)
			logger.info("not going to place order since max trade has been done for today");
		}
	}
	
	@ManagedOperation(description="To Reset Confitmation Recipt from Nordea")
	public synchronized void OrderPlacedSuccessfullyOnSOurce1(){
		logger.info("Confiramtion Recevied on Source1 trade");
		waitingConfirmationforSource1=false;
	}
	@ManagedOperation(description="To Reset Confitmation Recipt from Seb")
	public synchronized void OrderPlacedSuccessfullyOnSOurce2(){
		logger.info("Confiramtion Recevied on Source2 trade");
		waitingConfirmationforSource2=false;
	}

	private static int getHoursUntilTarget(int targetHour) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("CET"));
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		return hour < targetHour ? targetHour - hour : targetHour - hour + 24;
	}
}
