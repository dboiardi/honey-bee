package com.fm.fixconnector.algo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.camel.Exchange;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.fm.fixconnector.MessageController;
import com.fm.fixconnector.domain.AbstractMarketData;
import com.fm.fixconnector.nordea.MarketData;
import com.fm.fixconnector.seb.QuoteData;
import com.fm.fixconnector.util.Constants;
import com.fm.fixconnector.util.OrderTestMailSender;

@ManagedResource
public class PriceMatcher implements BeanNameAware {

	static final Logger logger = Logger.getLogger(PriceMatcher.class);
	private String name;

	// Source 1 is nordea
	// Source 2 is seb

	
	public OrderGateway orderGateway;
	
	double lastBidPriceSource1;
	double lastAskPriceSource1;

	double lastBidPriceSource2;
	double lastAskPriceSource2;
	
	private volatile AbstractMarketData bidPriceFromSource1;
	private volatile AbstractMarketData sellPriceFromSource1;

	private volatile AbstractMarketData bidPriceFromSource2;
	private volatile AbstractMarketData sellPriceFromSource2;

	private MessageController messageControllerSource1;
	private MessageController messageControllerSource2;

	private double multiplicationFactor = 1;
	private double differenceFactor = 0.03;

	public OrderTestMailSender orderTestMailSender;

	public OrderGateway getOrderGateway() {
		return orderGateway;
	}

	public void setOrderGateway(OrderGateway orderGateway) {
		this.orderGateway = orderGateway;
	}

	public MessageController getMessageControllerSource1() {
		return messageControllerSource1;
	}

	public void setMessageControllerSource1(
			MessageController messageControllerSource1) {
		this.messageControllerSource1 = messageControllerSource1;
	}

	public MessageController getMessageControllerSource2() {
		return messageControllerSource2;
	}

	public void setMessageControllerSource2(
			MessageController messageControllerSource2) {
		this.messageControllerSource2 = messageControllerSource2;
	}

	public double getMultiplicationFactor() {
		return multiplicationFactor;
	}

	public void setMultiplicationFactor(double multiplicationFactor) {
		this.multiplicationFactor = multiplicationFactor;
	}

	public double getDifferenceFactor() {
		return differenceFactor;
	}

	public void setDifferenceFactor(double differenceFactor) {
		this.differenceFactor = differenceFactor;
	}

	public OrderTestMailSender getOrderTestMailSender() {
		return orderTestMailSender;
	}

	public void setOrderTestMailSender(OrderTestMailSender orderTestMailSender) {
		this.orderTestMailSender = orderTestMailSender;
	}
	

	public void onMessageFromSource1(AbstractMarketData marketData) {
		logger.info(name +"Price  from Source 1 " + marketData);
		if (marketData.getQuoteId().endsWith("BID")) { // sell price in
																// marketData
			logger.info(name+"Sell Price from source 1 " + marketData);
			sellPriceFromSource1 = marketData;
			if (bidPriceFromSource2 == null) {
				logger.error(name+"Bid Price from Source 2 is Null "+name);
			} else {
				if (bidPriceFromSource2.getPrice() <= sellPriceFromSource1
						.getPrice() * multiplicationFactor) {
					if (differenceFactor == 0
							|| (differenceFactor != 0 && sellPriceFromSource1
									.getPrice()
									- bidPriceFromSource2.getPrice() >= differenceFactor && (sellPriceFromSource1.getPrice()!=lastAskPriceSource1 && bidPriceFromSource2.getPrice() !=lastBidPriceSource2))) {
						String text = "Going to place bid order on source 2 :"
								+ bidPriceFromSource2.getPrice()
								+ " and sell Order On Source 1 :"
								+ sellPriceFromSource1.getPrice();
						logger.info(name+text+" price detail on Source 2 "+bidPriceFromSource2+" Price detail of Source 1"+sellPriceFromSource1);
						lastAskPriceSource1=sellPriceFromSource1.getPrice();
						lastBidPriceSource2=bidPriceFromSource2.getPrice();
						orderGateway.placeOrder(messageControllerSource2,
								messageControllerSource1, bidPriceFromSource2,
								sellPriceFromSource1);
						/*orderTestMailSender.sendMail(bidPriceFromSource1,
								sellPriceFromSource1, bidPriceFromSource2,
								sellPriceFromSource2, text);
		*/			} else {
						logger.info(name+"Difference Factor is mearge"+ " price detail on Source 2 "+bidPriceFromSource2+" Price detail of Source 1"+sellPriceFromSource1);
					}
				} else {
					logger.info(name+"No order: Sell Price from Source 1 :"
							+ sellPriceFromSource1.getPrice() +" Order Type"+sellPriceFromSource1
							+ " is less than Bid Price from Source 2 :"
							+ bidPriceFromSource2.getPrice() +"Order Type "+bidPriceFromSource2);
				}
			}
		}
		if (marketData.getQuoteId().endsWith("ASK")) { // bid price in
																// marketdata
			logger.info(name+"Bid Price from source 1 in Conext of our app" + marketData);
			bidPriceFromSource1 = marketData;
			if (sellPriceFromSource2 == null) {
				logger.error("Sell Price from Source 2 is Null "+ name);
			} else {
				if (bidPriceFromSource1.getPrice() <= sellPriceFromSource2
						.getPrice() * multiplicationFactor) {
					if (differenceFactor == 0
							|| (differenceFactor != 0 && sellPriceFromSource2
									.getPrice()
									- bidPriceFromSource1.getPrice() >= differenceFactor && (sellPriceFromSource2.getPrice()!=lastAskPriceSource2 && bidPriceFromSource1.getPrice() !=lastBidPriceSource1) )) {
						String text = "Going to place bid order on source 1 :"
								+ bidPriceFromSource1.getPrice()
								+ " and sell Order On Source 2 :"
								+ sellPriceFromSource2.getPrice();
						logger.info(name+text+" price detail on Source 1 "+bidPriceFromSource1+" Price detail of Source 2"+sellPriceFromSource2);
						lastAskPriceSource2=sellPriceFromSource2.getPrice();
						lastBidPriceSource1=bidPriceFromSource1.getPrice();
						orderGateway.placeOrder(messageControllerSource1,
								messageControllerSource2, bidPriceFromSource1,
								sellPriceFromSource2);
		/*				orderTestMailSender.sendMail(bidPriceFromSource1,
								sellPriceFromSource1, bidPriceFromSource2,
								sellPriceFromSource2, text);
		*/			} else {
						logger.info(name+"Difference Factor is mearge"+" price detail on Source 1 "+bidPriceFromSource1+" Price detail of Source 2"+sellPriceFromSource2);
					}
				} else {
					logger.info(name+"No order: Bid Price from Source 1 :"
							+ bidPriceFromSource1.getPrice()+"Order Type "+bidPriceFromSource1
							+ " is more than Sell Price from Source 2 :"
							+ sellPriceFromSource2.getPrice()+" Order Type "+sellPriceFromSource2 );
				}
			}

		}

	}

	public void onMessageFromSource2(Exchange exchange) {
		QuoteData marketData = (QuoteData) exchange.getIn().getBody();
		System.out.println("Price  from Source 2 " + marketData);
		if ("1".equalsIgnoreCase(marketData.getPriceType())) { // bid price in
																// marketData
			logger.info(name+" Bid Price from source 2" + marketData);
			bidPriceFromSource2 = marketData;
			if (sellPriceFromSource1 == null) {
				logger.error(name+" Sell Price from Source 1 is Null" +name);
			} else {
				if (bidPriceFromSource2.getPrice() <= sellPriceFromSource1
						.getPrice() * multiplicationFactor) {
					if (differenceFactor == 0
							|| (differenceFactor != 0 && sellPriceFromSource1
									.getPrice()
									- bidPriceFromSource2.getPrice() >= differenceFactor) && (sellPriceFromSource1.getPrice()!=lastAskPriceSource1 && bidPriceFromSource2.getPrice() !=lastBidPriceSource2)) {
						String text = "Going to place bid order on source 2 :"
								+ bidPriceFromSource2.getPrice()
								+ " and sell Order On Source 1 :"
								+ sellPriceFromSource1.getPrice();
						logger.info(name+text+" price detail on Source 2 "+bidPriceFromSource2+" Price detail of Source 1"+sellPriceFromSource1);
					lastAskPriceSource1=sellPriceFromSource1.getPrice();
					lastBidPriceSource2=bidPriceFromSource2.getPrice();
						orderGateway.placeOrder(messageControllerSource2,
								messageControllerSource1, bidPriceFromSource2,
								sellPriceFromSource1);
		/*				orderTestMailSender.sendMail(bidPriceFromSource1,
								sellPriceFromSource1, bidPriceFromSource2,
								sellPriceFromSource2, text);
		*/			} else {
						logger.info(name+"Difference Factor is mearge"+" price detail on Source 1 "+bidPriceFromSource1+" Price detail of Source 2"+sellPriceFromSource2);
					}
				} else {
					logger.info(name+"No order: Bid Price from Source 2 :"
							+ bidPriceFromSource2.getPrice()
							+ " is more than Sell Price from Source 1 :"
							+ sellPriceFromSource1.getPrice());
				}
			}
		}
		if ("2".equalsIgnoreCase(marketData.getPriceType())) { // sell price in
																// market data
			logger.info(name+"Sell Price from source 2 " + marketData);
			sellPriceFromSource2 = marketData;
			if (bidPriceFromSource1 == null) {
				logger.error(name+"Bid Price from Source 1 is Null" +name);
			} else {
				if (bidPriceFromSource1.getPrice() <= sellPriceFromSource2
						.getPrice() * multiplicationFactor) {
					if (differenceFactor == 0
							|| (differenceFactor != 0 && sellPriceFromSource2
									.getPrice()
									- bidPriceFromSource1.getPrice() >= differenceFactor) && (sellPriceFromSource2.getPrice()!=lastAskPriceSource2 && bidPriceFromSource1.getPrice() !=lastBidPriceSource1)) {
						String text = "Going to place bid order on source 1 :"
								+ bidPriceFromSource1.getPrice()
								+ " and sell Order On Source 2 :"
								+ sellPriceFromSource2.getPrice();
						logger.info(name+text+" price detail on Source 1 "+bidPriceFromSource1+" Price detail of Source 2"+sellPriceFromSource2);
						lastAskPriceSource2=sellPriceFromSource2.getPrice();
						lastBidPriceSource1=bidPriceFromSource1.getPrice();
						orderGateway.placeOrder(messageControllerSource1,
								messageControllerSource2, bidPriceFromSource1,
								sellPriceFromSource2);
		/*				orderTestMailSender.sendMail(bidPriceFromSource1,
								sellPriceFromSource1, bidPriceFromSource2,
								sellPriceFromSource2, text);
		*/			} else {
						logger.info(name+"Difference Factor is mearge"+" price detail on Source 1 "+bidPriceFromSource1+" Price detail of Source 2"+sellPriceFromSource2);
					}
				} else {
					logger.info(name+"No order: Sell Price from Source 2 "
							+ sellPriceFromSource2.getPrice()
							+ " is less than Bid Price from Source 1 "
							+ bidPriceFromSource1.getPrice());
				}
			}
		}
	}
	@ManagedOperation(description="To Get html Scrap data.")
	public void placeBidMessageonSource1(){
		orderGateway.placeOrder(messageControllerSource1,
				messageControllerSource2, bidPriceFromSource1,
				sellPriceFromSource2);
	}
	/*@ManagedOperation(description="To Get html Scrap data.")
	public void placeSellMessageonSource1(){
		orderGateway.placeOrder(messageControllerSource2,
				messageControllerSource1, bidPriceFromSource2,
				sellPriceFromSource1);
	}*/
	@ManagedOperation(description="To Get html Scrap data.")
	public void placeBidMessageonSource2(){
		orderGateway.placeOrder(messageControllerSource2,
				messageControllerSource1, bidPriceFromSource2,
				sellPriceFromSource1);
	}
	
	@ManagedOperation(description="To Set Difference facotr .")
	public void setDiffFactor(double diff){
		differenceFactor=diff;
	}
	
	public ArrayList<Object> getBestPriceMessageController(AbstractMarketData marketData) {
		ArrayList<Object> resturnList=new ArrayList<Object>();
		logger.info("Querying for Best Price becuase of Rejected ot partially filled order");
		if ((marketData.getQuoteId().endsWith("ASK") && marketData instanceof MarketData) || ("2".equalsIgnoreCase(marketData.getPriceType()) && marketData instanceof QuoteData)) {
			if (bidPriceFromSource1.getPrice() > bidPriceFromSource2.getPrice()) {
				resturnList.add(messageControllerSource2);
				resturnList.add(bidPriceFromSource2);
			} else {
				resturnList.add(messageControllerSource1);
				resturnList.add(bidPriceFromSource1);
			}
		} else if((marketData.getQuoteId().endsWith("BID") && marketData instanceof MarketData) || ("1".equalsIgnoreCase(marketData.getPriceType()) && marketData instanceof QuoteData)) {
			if (sellPriceFromSource1.getPrice() > sellPriceFromSource2
					.getPrice()) {
				
				resturnList.add(messageControllerSource1);
				resturnList.add(sellPriceFromSource1);
			} else {
				resturnList.add(messageControllerSource2);
				resturnList.add(sellPriceFromSource2);
				
			}
		}else return resturnList;
		return resturnList;
	}

	public ArrayList<Object> getPriceMessageController(AbstractMarketData marketData) {
		ArrayList<Object> resturnList=new ArrayList<Object>();
		logger.info(name+"Querying for  Price becuase of Rejected ot partially filled order");
		if ((marketData.getQuoteId().endsWith("ASK") && marketData instanceof MarketData) ) {
		if(bidPriceFromSource1.getPrice()!=lastBidPriceSource1){
					resturnList.add(messageControllerSource1);
						resturnList.add(bidPriceFromSource1);
						lastBidPriceSource1=bidPriceFromSource1.getPrice();
				 }
		}
			
			 else if((marketData.getQuoteId().endsWith("BID") && marketData instanceof MarketData)){
				if(sellPriceFromSource1.getPrice()!=lastAskPriceSource1){
					resturnList.add(messageControllerSource1);
					resturnList.add(sellPriceFromSource1);	
					lastAskPriceSource1=sellPriceFromSource1.getPrice();
				}
		}
		 else if( ("1".equalsIgnoreCase(marketData.getPriceType()) && marketData instanceof QuoteData)) {
				if(sellPriceFromSource1.getPrice()!=lastAskPriceSource1){
			 resturnList.add(messageControllerSource2);
				resturnList.add(sellPriceFromSource2);
				lastAskPriceSource1=sellPriceFromSource1.getPrice();
				}
		} else if( ("2".equalsIgnoreCase(marketData.getPriceType()) && marketData instanceof QuoteData)){
			if(bidPriceFromSource2.getPrice()!=lastBidPriceSource2){
				resturnList.add(messageControllerSource2);
				resturnList.add(bidPriceFromSource2);
				lastBidPriceSource2=bidPriceFromSource2.getPrice();
			}
		}
		return resturnList;
	}

	public void setBeanName(String arg0) {
		logger.info("Called with"+arg0);
		name=arg0;
		
	}

}
