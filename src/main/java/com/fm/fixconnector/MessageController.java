package com.fm.fixconnector;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Producer;
import org.apache.camel.component.quickfixj.MessagePredicate;
import org.apache.camel.component.quickfixj.QuickfixjProducer;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;

import quickfix.SessionID;
import quickfix.field.MsgType;

public class MessageController implements IMessageController {

	static final Logger logger = Logger.getLogger(MessageController.class);
	ApplicationContext context;
	CamelContext camelContext;
	
	String gatewayUri = "";
	Producer producer = null;
	String contextUri = "";

	public String getContextUri() {
		return contextUri;
	}

	public void setContextUri(String contextUri) {
		this.contextUri = contextUri;
	}

	public String getGatewayUri() {
		return gatewayUri;
	}

	public void setGatewayUri(String gatewayUri) {
		this.gatewayUri = gatewayUri;
	}
	Endpoint gatewayEndpoint;
	
	public void init() {
		this.camelContext = (CamelContext) context.getBean(contextUri);
		 gatewayEndpoint = camelContext.getEndpoint(gatewayUri);
	}

	public void sendMessage(Object message) {
		
		try {
			Exchange exchange=null;
			producer = gatewayEndpoint.createProducer();
			exchange = producer.createExchange(ExchangePattern.InOut);
			logger.info("Sending  message "+message);
			exchange.getIn().setBody(message);
			producer.process(exchange);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Following exception  "+e.getMessage()+e.getStackTrace());
		}

	}

	public void setApplicationContext(ApplicationContext context)
			throws BeansException {
		this.context = context;
	}

}
