package com.fm.fixconnector.analytics;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.joda.time.DateTime;

import com.fm.fixconnector.domain.PriceType;

public class StatisticsAnalytics extends Analytics{

	private String currencyPair;
	private PriceType priceType;
	private double variance;
	private double deviation;
	private double sigma1;
	private double sigma2;
	private double sigma3;
	private String liquidityProvider;
	
	public StatisticsAnalytics(String currencyPair, PriceType priceType,
			double variance, double deviation, double sigma1, double sigma2,
			double sigma3,DateTime dateTime,String liquidityProvider) {
		super();
		setDateTime(dateTime);
		this.currencyPair = currencyPair;
		this.priceType = priceType;
		this.variance = variance;
		this.deviation = deviation;
		this.sigma1 = sigma1;
		this.sigma2 = sigma2;
		this.sigma3 = sigma3;
		this.liquidityProvider=liquidityProvider;
	}

	public String getCurrencyPair() {
		return currencyPair;
	}

	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair;
	}

	public PriceType getPriceType() {
		return priceType;
	}

	public void setPriceType(PriceType priceType) {
		this.priceType = priceType;
	}

	public double getVariance() {
		return variance;
	}

	public void setVariance(double variance) {
		this.variance = variance;
	}

	public double getDeviation() {
		return deviation;
	}

	public void setDeviation(double deviation) {
		this.deviation = deviation;
	}

	public double getSigma1() {
		return sigma1;
	}

	public void setSigma1(double sigma1) {
		this.sigma1 = sigma1;
	}

	public double getSigma2() {
		return sigma2;
	}

	public void setSigma2(double sigma2) {
		this.sigma2 = sigma2;
	}

	public double getSigma3() {
		return sigma3;
	}

	public void setSigma3(double sigma3) {
		this.sigma3 = sigma3;
	}

	public String getLiquidityProvider() {
		return liquidityProvider;
	}

	public void setLiquidityProvider(String liquidityProvider) {
		this.liquidityProvider = liquidityProvider;
	}
	
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	

}
