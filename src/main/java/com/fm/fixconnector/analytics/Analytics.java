package com.fm.fixconnector.analytics;

import org.joda.time.DateTime;

public abstract class Analytics {

	private DateTime dateTime;

	public DateTime getDateTime() {
		return dateTime;
	}
	
	public void setDateTime(DateTime dateTime) {
		this.dateTime = dateTime;
	}
}
