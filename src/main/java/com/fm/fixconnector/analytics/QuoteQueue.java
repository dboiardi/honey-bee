package com.fm.fixconnector.analytics;

import org.joda.time.DateTime;

public class QuoteQueue extends Analytics {

	private String strategyName;
	private int queueSize;
	
	public QuoteQueue(String strategyName, int queueSize,DateTime dateTime) {
		super();
		setDateTime(dateTime);
		this.strategyName = strategyName;
		this.queueSize = queueSize;
	}
	
	public String getStrategyName() {
		return strategyName;
	}
	public void setStrategyName(String strategyName) {
		this.strategyName = strategyName;
	}
	public int getQueueSize() {
		return queueSize;
	}
	public void setQueueSize(int queueSize) {
		this.queueSize = queueSize;
	}
	
	
	
}
