package com.fm.fixconnector.analytics;

import org.joda.time.DateTime;

public class QuoteFrequency extends Analytics {

	private String currecnyPair;
	private long noOfPriceUpdate;
	private long timeInterval;
	private String timeUnit;
	
	public QuoteFrequency(String currecnyPair, long noOfPriceUpdate,DateTime dateTime,long timeInterval,String timeUnit) {
		super();
		setDateTime(dateTime);
		this.currecnyPair = currecnyPair;
		this.noOfPriceUpdate = noOfPriceUpdate;
		this.timeInterval=timeInterval;
		this.timeUnit=timeUnit;
	}
	
	public String getCurrecnyPair() {
		return currecnyPair;
	}
	public void setCurrecnyPair(String currecnyPair) {
		this.currecnyPair = currecnyPair;
	}
	public long getNoOfPriceUpdate() {
		return noOfPriceUpdate;
	}
	public void setNoOfPriceUpdate(long noOfPriceUpdate) {
		this.noOfPriceUpdate = noOfPriceUpdate;
	}
	
	
}
