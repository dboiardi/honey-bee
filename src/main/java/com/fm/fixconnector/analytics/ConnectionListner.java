package com.fm.fixconnector.analytics;

import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.joda.time.DateTime;

import com.fm.fixconnector.database.GenericDAO;
import com.fm.fixconnector.domain.LogonMessage;
import com.fm.fixconnector.util.DateUtil;

public class ConnectionListner {

	GenericDAO genericDAO;
	
	public void onNewConnection(LogonMessage message){
		Date messageTime=DateUtil.getFormatedSendingTime(message.getSendingDateTime());
		ConnectionAnalytics analytics=new ConnectionAnalytics(message.getHeader().getSendCompId()+"-"+message.getHeader().getTargetCompID(),messageTime!=null?new DateTime(messageTime.getTime()):null);
		genericDAO.saveObject(analytics);
	}
	

	public GenericDAO getGenericDAO() {
		return genericDAO;
	}

	public void setGenericDAO(GenericDAO genericDAO) {
		this.genericDAO = genericDAO;
	}
	
	
}
