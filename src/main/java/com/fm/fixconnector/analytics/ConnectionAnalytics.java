package com.fm.fixconnector.analytics;

import org.joda.time.DateTime;

public class ConnectionAnalytics extends Analytics {

	private String connectionId;

	public ConnectionAnalytics(String conectionId,DateTime dateTime) {
		this.connectionId=conectionId;
		setDateTime(dateTime);
	}
	
	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	
}
