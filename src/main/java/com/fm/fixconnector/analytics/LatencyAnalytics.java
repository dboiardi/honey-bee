package com.fm.fixconnector.analytics;

import org.joda.time.DateTime;

public class LatencyAnalytics extends Analytics{

	private String currencyPair;
	private String liquidityProvider;
	private DateTime serverTime;
	private DateTime localTime;
	private long latency;
	private String unit;
	
	public LatencyAnalytics(String currencyPair, String liquidityProvider,
			DateTime serverTime, DateTime localTime, long latency, String unit) {
		super();
		this.currencyPair = currencyPair;
		this.liquidityProvider = liquidityProvider;
		this.serverTime = serverTime;
		this.localTime = localTime;
		this.latency = latency;
		this.unit= unit;
	}
	
	public String getCurrencyPair() {
		return currencyPair;
	}
	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair;
	}
	public String getLiquidityProvider() {
		return liquidityProvider;
	}
	public void setLiquidityProvider(String liquidityProvider) {
		this.liquidityProvider = liquidityProvider;
	}
	public DateTime getServerTime() {
		return serverTime;
	}
	public void setServerTime(DateTime serverTime) {
		this.serverTime = serverTime;
	}
	public DateTime getLocalTime() {
		return localTime;
	}
	public void setLocalTime(DateTime localTime) {
		this.localTime = localTime;
	}
	public long getLatency() {
		return latency;
	}
	public void setLatency(long latency) {
		this.latency = latency;
	}
	
	
	
}
