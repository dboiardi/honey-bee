package com.fm.fixconnector.repo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;











import org.joda.time.DateTimeZone;
import org.springframework.util.StopWatch;

import com.fm.fixconnector.nordea.MarketData;
import com.fm.fixconnector.util.CustomThreadName;
import com.fm.fixconnector.analytics.LatencyAnalytics;
import com.fm.fixconnector.database.GenericDAO;
import com.fm.fixconnector.domain.PriceType;
import com.fm.fixconnector.domain.PriceUpdate;
import com.fm.fixconnector.initiator.ConfigManager;
import com.fm.fixconnector.jobs.LastSecondTickClearnerThread;
import com.fm.fixconnector.jobs.PriceUpdateMonitorJob;
import com.fm.fixconnector.listner.Listner;

public class PriceRepo {

	static final Logger logger = Logger.getLogger(PriceRepo.class);
	
	private static ScheduledExecutorService executorService;
	private GenericDAO genericDAO;
	private Thread marketDataConsumerThread;
	private List<Listner<PriceUpdate>> listner;
	private  ArrayBlockingQueue priceQueue=new ArrayBlockingQueue(100);
	private final static String BID="1";
	private final static String ASK="0";
	private long interval=0;
	
	private static ConcurrentHashMap<MultiKey, MarketData> marketPriceMap=new ConcurrentHashMap<>(); 
	
	private ConcurrentHashMap<String, PriceUpdate> recentPriceUpdateMap=new ConcurrentHashMap<String, PriceUpdate>();
	
	private SimpleDateFormat formatter=new SimpleDateFormat("yyyyMMdd-HH:mm:ss.SSS");
	private ConcurrentHashMap<String, AtomicInteger> noOfTicksInLastSec=new ConcurrentHashMap<>();
	private ConcurrentHashMap<String, AtomicInteger> noOfTicksInCurrentSec=new ConcurrentHashMap<>();
	private long avgLatency;
	private ConcurrentHashMap<String, DateTime> lastUpdate=new ConcurrentHashMap<>();
	private long minQuantityForPriceUpdate=1000001;
	
	//  CurrencyPair -> Liqudity Provider -> Position
	private ConcurrentHashMap<String, ConcurrentHashMap<String,Long>> bidSizeMap=new ConcurrentHashMap<String, ConcurrentHashMap<String,Long>>();
	private ConcurrentHashMap<String, ConcurrentHashMap<String,Long>> askSizeMap=new ConcurrentHashMap<String, ConcurrentHashMap<String,Long>>();
	
	
	public void init(){
		executorService=Executors.newScheduledThreadPool(2,new CustomThreadName("PriceMonitorLastUpdated"));
		executorService.scheduleAtFixedRate(new PriceUpdateMonitorJob(this,genericDAO,1000,TimeUnit.MILLISECONDS.name()), 100, 1000, TimeUnit.MILLISECONDS);
		executorService.scheduleAtFixedRate(new LastSecondTickClearnerThread(this), 0, 1000, TimeUnit.MILLISECONDS);
		
	}



	public void startMarketDataConsumerThread() {
		if(marketDataConsumerThread==null || (marketDataConsumerThread!=null && (!marketDataConsumerThread.isAlive() && marketDataConsumerThread.isInterrupted()))){
			
		Runnable runnable=new Runnable() {
			
			public void run(){
				MarketData data;
				try {
					data = (MarketData) priceQueue.take();
					while(data!=null){
						
						try{
						updateMarketData(data);	
						data=(MarketData) priceQueue.take();
						}catch(Exception exception){
							exception.printStackTrace();
							logger.error(" Rerunning Thread ..."+ExceptionUtils.getFullStackTrace(exception));
						}
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
					logger.error(" Rerunning Thread ...");
				
				}
			logger.error(" Market Thread Terminated ...");	
			}
		};
		
		marketDataConsumerThread=(new Thread(runnable));
		marketDataConsumerThread.setName("MarketDataConsumer");
		marketDataConsumerThread.start();
		}else{
			logger.info("Not starting market data cosnumer Thread "+marketDataConsumerThread!=null?marketDataConsumerThread.isAlive()+" Intreputed "+marketDataConsumerThread.isInterrupted():"Dead" );
		}
	}
	
	public MarketData addPriceToQueue(MarketData marketData){
			
		logger.debug("Getting new prices "+marketData.getPrice()+"  "+marketData);
		printLatency(marketData);
		boolean status=priceQueue.offer(marketData);
		
		if(!status){
				logger.info("Price Queue size is "+priceQueue.size());
				priceQueue.clear();
		}
				
		return marketData;
	}
	
	
	private void printLatency(MarketData marketData) {
		Long latency=(new Date().getTime()-getFormatedSendingTime(marketData.getSendingTime()).getTime());
		avgLatency=(avgLatency+latency)/2;
		logger.info("Latecny is "+ latency+"ms"+" avg Latency is "+avgLatency);
		DateTime localTime=DateTime.now(DateTimeZone.UTC);
		LatencyAnalytics latencyAnalytics=new LatencyAnalytics(marketData.getCurrency(), marketData.getMdEntryOriginator(), new DateTime(getFormatedSendingTime(marketData.getSendingTime()),DateTimeZone.UTC), localTime, (localTime.getMillis()-getFormatedSendingTime(marketData.getSendingTime()).getTime()),TimeUnit.MILLISECONDS.name());
		genericDAO.saveObject(latencyAnalytics);
	}

	
	public Date getFormatedSendingTime(String time) {
		try {
			return formatter.parse(time);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	// This would be called by Thread
	public void updateMarketData(MarketData data){
		
		try{
		
		if(isPriceRequireUpdate(data)){
		
		updateTicksInLastSecond(data);
		
		
	//	updatePrice(data);
		updateRawPrice(data);
	//	updatePosition(data);
		
		
		
		String curr=data.getCurrency();
		PriceUpdate lastUpdate=recentPriceUpdateMap.get(curr);
		
		if(lastUpdate.isUpdated()){

			
			logger.info("Sending updated for "+curr+" Laste Update "+lastUpdate);	
			for(Listner<PriceUpdate> priceListner:listner){
					priceListner.onMessage(lastUpdate);
			}	
			
			if(lastUpdate.getBestAskPrice()!=0 && lastUpdate.getBestBidPrice()!=0){
				genericDAO.saveObject(lastUpdate);	
			}
			lastUpdate.setUpdated(false);
			
			//genericDAO.saveObject(data);
			
		}else{
			logger.info("Not Sending updated for "+curr+" Laste Update "+lastUpdate);
		}
		
		
			}
		}catch(Exception exception){
			logger.error("Errro in Price Repo Thread "+ExceptionUtils.getFullStackTrace(exception));
		}
	}
	

	private void updateTicksInLastSecond(MarketData data) {
		
		AtomicInteger atomicInteger= noOfTicksInCurrentSec.get(data.getCurrency());
		if(atomicInteger!=null){
			atomicInteger.incrementAndGet();
		}else{
			AtomicInteger atomicInteger2=new AtomicInteger(1);	
			noOfTicksInCurrentSec.put(data.getCurrency(), atomicInteger2)	;
		}
	}


	public void clearTicksInLastSecond(){
		noOfTicksInLastSec.clear();
		noOfTicksInLastSec.putAll(noOfTicksInCurrentSec);
		noOfTicksInCurrentSec.clear();
	}
	
	public int getUpdateInLastSecond(String currecnyPair){
		AtomicInteger atomicInteger=noOfTicksInLastSec.get(currecnyPair);
		if(atomicInteger!=null){
			return atomicInteger.get();
		}else{
			return 0;	
		}
	}

	private boolean isPriceRequireUpdate(PriceUpdate priceUpdate,String side){
		DateTime currentTime = DateTime.now();
		if(BID.equalsIgnoreCase(side) && currentTime.getMillis()-priceUpdate.getBidpriceUpdateTime().getMillis() >ConfigManager.getConfig().getPriceReplaceTimeInMS() ){
			return true;
		}else if(ASK.equalsIgnoreCase(side) && currentTime.getMillis()-priceUpdate.getAskpriceUpdateTime().getMillis() >ConfigManager.getConfig().getPriceReplaceTimeInMS() ){
			return true;
		}
		
		return false;
	}
	
	public PriceUpdate getPriceUpdate(String currecnyPair){
		return recentPriceUpdateMap.get(currecnyPair.replaceAll("/", "_"));
	}
	
	public double getPriceForCuurencyAsPerPriceType(String currecnyPair,PriceType priceType){
		
		PriceUpdate priceUpdate=getPriceUpdate(currecnyPair);
		if(priceUpdate!=null){
		if(priceType==PriceType.BID)
			return priceUpdate.getBestBidPrice();
		else
			return priceUpdate.getBestAskPrice();
		}
		return  0;
	}
	
	private boolean isPriceRequireUpdate(MarketData marketData){
		
		boolean result = true;
		MarketData lastMarketData=marketPriceMap.get(new MultiKey(marketData.getCurrency(),marketData.getPriceType()));
		if(lastMarketData==null ||( lastMarketData!=null && ( lastMarketData.getQuantity() != marketData.getQuantity() || lastMarketData.getPrice() != marketData.getPrice()))){
			marketPriceMap.put(new MultiKey(marketData.getCurrency(),marketData.getPriceType()), marketData);	
			return true;
		}else {
			logger.info("Old Price Recevied last Market data"+lastMarketData +" Market data" +marketData);
			return false;
		}
		
	}
	
	public void updateRawPrice(MarketData data){
		

		PriceUpdate lastPriceUpdate=recentPriceUpdateMap.get(data.getCurrency());
		if(isBidData(data)){
		// Rule 1  Price change for Qty <=1,000,000
			if( lastPriceUpdate!=null  && data.getQuantity() <minQuantityForPriceUpdate /*&& (data.getPrice()>lastPriceUpdate.getBestBidPrice()  )*/ ){
				lastPriceUpdate.setBestBidPrice(data.getPrice() );	
				lastPriceUpdate.setBestbidSize(data.getQuantity());
				lastPriceUpdate.setBestBidLP(data.getMdEntryOriginator());
				lastPriceUpdate.setBidpriceUpdateTime(DateTime.now());
				lastPriceUpdate.setUpdated(true);
				
			}
			
			else if(lastPriceUpdate==null  && data.getQuantity() <minQuantityForPriceUpdate){
				lastPriceUpdate=new PriceUpdate(data.getCurrency(), 0, 0, data.getQuantity(), 0, data.getPrice(), 0, data.getMdEntryOriginator(), "");
				lastPriceUpdate.setUpdated(true);
				recentPriceUpdateMap.put(data.getCurrency(), lastPriceUpdate);
				lastPriceUpdate.setBidpriceUpdateTime(DateTime.now());
				//Put Debug logging
			}
			// Rule 2 Rule 2: Price hasn't changed and Qty is greater, update Qty available
			 if( lastPriceUpdate !=null && data.getQuantity()> minQuantityForPriceUpdate && data.getPrice() == lastPriceUpdate.getBestBidPrice() ){
					lastPriceUpdate.setBestbidSize(data.getQuantity());
					lastPriceUpdate.setBestBidLP(data.getMdEntryOriginator());
					lastPriceUpdate.setBidpriceUpdateTime(DateTime.now());
					lastPriceUpdate.setUpdated(true);
				}
			// Rule 3 : If price is BETTER, take any Qty
			 
			 if( lastPriceUpdate !=null &&  data.getPrice() > lastPriceUpdate.getBestBidPrice() ){
				
				 	lastPriceUpdate.setBestBidPrice(data.getPrice() );	
					lastPriceUpdate.setBestbidSize(data.getQuantity());
					lastPriceUpdate.setBestBidLP(data.getMdEntryOriginator());
					lastPriceUpdate.setBidpriceUpdateTime(DateTime.now());
					lastPriceUpdate.setUpdated(true);
					
					
				}
		}else{
			
			if(lastPriceUpdate!=null && data.getQuantity() <minQuantityForPriceUpdate /*&& (data.getPrice()<lastPriceUpdate.getBestAskPrice() )*/ ){
				
				lastPriceUpdate.setBestAskPrice(data.getPrice() );	
				lastPriceUpdate.setBestaskSize(data.getQuantity());
				lastPriceUpdate.setBestASkLP(data.getMdEntryOriginator());
				lastPriceUpdate.setAskpriceUpdateTime(DateTime.now());
				lastPriceUpdate.setUpdated(true);
				
			}
			
			else if(lastPriceUpdate==null && data.getQuantity() <minQuantityForPriceUpdate){
				//Put Debug logging
				lastPriceUpdate=new PriceUpdate(data.getCurrency(), 0, 0, 0, data.getQuantity(), 0,data.getPrice(), "", data.getMdEntryOriginator());
				lastPriceUpdate.setAskpriceUpdateTime(DateTime.now());
				recentPriceUpdateMap.put(data.getCurrency(), lastPriceUpdate);
				lastPriceUpdate.setUpdated(true);
			}
			
			if( lastPriceUpdate !=null &&data.getQuantity() > minQuantityForPriceUpdate  && data.getPrice() == lastPriceUpdate.getBestAskPrice()){
				
				lastPriceUpdate.setBestaskSize(data.getQuantity());
				lastPriceUpdate.setBestASkLP(data.getMdEntryOriginator());
				lastPriceUpdate.setAskpriceUpdateTime(DateTime.now());
				lastPriceUpdate.setUpdated(true);
				
			}
			
			// Rule 3 : If price is BETTER, take any Qty
			 
			 if( lastPriceUpdate !=null &&  data.getPrice() < lastPriceUpdate.getBestAskPrice() ){
				
					lastPriceUpdate.setBestAskPrice(data.getPrice() );	
					lastPriceUpdate.setBestaskSize(data.getQuantity());
					lastPriceUpdate.setBestASkLP(data.getMdEntryOriginator());
					lastPriceUpdate.setAskpriceUpdateTime(DateTime.now());
					lastPriceUpdate.setUpdated(true);
				
			}
		}
		
	}
	
	private Long getBestPositionForInstrument(String  curr,String side){
		PriceUpdate lastPriceUpdate=recentPriceUpdateMap.get(curr);
		if(BID.equalsIgnoreCase(side)){
			 return lastPriceUpdate !=null ? lastPriceUpdate.getBestbidSize():0;	
		}else{
			return lastPriceUpdate !=null ? lastPriceUpdate.getBestaskSize():0;
		}
		
	}
	
	private double getBestPriceForInstrument(String curr, String side){
		
		PriceUpdate lastPriceUpdate=recentPriceUpdateMap.get(curr);
		if(BID.equalsIgnoreCase(side)){
			 return lastPriceUpdate!=null  ? lastPriceUpdate.getBestBidPrice():0;	
		}else{
			return lastPriceUpdate !=null  ? lastPriceUpdate.getBestAskPrice():0;
		}
		
	}
	
	private String getBestLPForInstrument(String  curr,String side){
		PriceUpdate lastPriceUpdate=recentPriceUpdateMap.get(curr);
		if(BID.equalsIgnoreCase(side)){
			 return lastPriceUpdate !=null ? lastPriceUpdate.getBestBidLP():"";	
		}else{
			 return lastPriceUpdate !=null ? lastPriceUpdate.getBestASkLP():"";
		}
		
	}
	private Long getTotalPositionForInstrument(String  curr,String side){
		ConcurrentHashMap<String,Long> lastSizeMap;
		Long retValue=0l;
		if(BID.equalsIgnoreCase(side)){
			 lastSizeMap = bidSizeMap.get(curr);	
		}else{
			 lastSizeMap = askSizeMap.get(curr);
		}
		if(lastSizeMap!=null){
			
			for(Long position:lastSizeMap.values()){
				retValue=retValue+position;
			}
		}else{
			
			// Log error no Position found;
			
		}
		return retValue;
		
		
	}
	
	private void updatePosition(MarketData data){
		
		if(isBidData(data)){
			ConcurrentHashMap<String,Long> lastSizeMap=bidSizeMap.get(data.getCurrency());
			if(lastSizeMap!=null){
				lastSizeMap.put(data.getMdEntryOriginator(), data.getQuantity());
			}else{
				ConcurrentHashMap<String, Long> positionMapForLiquidityProvider=new ConcurrentHashMap<>();
				positionMapForLiquidityProvider.put(data.getMdEntryOriginator(), data.getQuantity());
				bidSizeMap.put(data.getCurrency(), positionMapForLiquidityProvider);
			}
			
		}else{
			ConcurrentHashMap<String,Long> lastSizeMap=askSizeMap.get(data.getCurrency());
			if(lastSizeMap!=null){
				lastSizeMap.put(data.getMdEntryOriginator(), data.getQuantity());
			}else{
				ConcurrentHashMap<String, Long> positionMapForLiquidityProvider=new ConcurrentHashMap<>();
				positionMapForLiquidityProvider.put(data.getMdEntryOriginator(), data.getQuantity());
				askSizeMap.put(data.getCurrency(), positionMapForLiquidityProvider);
			}
		}
	
		
	}
	
	
	// O - Bid of Provider so Ask for us  same in case of 1
	// This will return status according to US
	private boolean isBidData(MarketData data){
		if("0".equalsIgnoreCase(data.getPriceType())){
			return true;
		}else{
			return false;
		}
	}

	public List<Listner<PriceUpdate>> getListner() {
		return listner;
	}

	public void setListner(List<Listner<PriceUpdate>> listner) {
		this.listner = listner;
	}

	public ConcurrentHashMap<String, PriceUpdate> getRecentPriceUpdateMap() {
		return recentPriceUpdateMap;
	}

	public ArrayBlockingQueue getPriceQueue() {
		return priceQueue;
	}
	
	public void setPriceQueue(ArrayBlockingQueue priceQueue) {
		this.priceQueue = priceQueue;
	}
	
	private void updateTimeStamp(MarketData data) {
		lastUpdate.put(data.getCurrency(),DateTime.now());	
	}


	public GenericDAO getGenericDAO() {
		return genericDAO;
	}

	public void setGenericDAO(GenericDAO genericDAO) {
		this.genericDAO = genericDAO;
	}
	
	
}
