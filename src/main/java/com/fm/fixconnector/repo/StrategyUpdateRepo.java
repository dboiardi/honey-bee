package com.fm.fixconnector.repo;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.fm.fixconnector.domain.StrategyUpdate;

public class StrategyUpdateRepo {

	private ObjectMapper mapper = new ObjectMapper();
	// Stargey Name - Currency Pair
	private HashMap<String, Map<String, StrategyUpdate>> strategyUpdatesMap = new HashMap<>();

	public void addStrategyValue(StrategyUpdate StrategyUpdate) {
		String strategyName = StrategyUpdate.getStrategyName();
		String currPair = StrategyUpdate.getCurrencyPair();
		Map<String, StrategyUpdate> currUpdate=strategyUpdatesMap.get(strategyName);
		if(currUpdate!=null){
			currUpdate.put(currPair, StrategyUpdate);
		}else{
			Map<String, StrategyUpdate> newCurrUpdate=new HashMap<String, StrategyUpdate>();
			newCurrUpdate.put(strategyName, StrategyUpdate);
			strategyUpdatesMap.put(strategyName, newCurrUpdate);
		}
		
	}

	public String getByName(String StrategyName)
			throws JsonGenerationException, JsonMappingException, IOException {
		if (strategyUpdatesMap.get(StrategyName) != null) {
			return mapper.writeValueAsString(strategyUpdatesMap
					.get(StrategyName));
		}
		return null;
	}

	public String getByNameAndPair(String StrategyName, String CurrPair)
			throws JsonGenerationException, JsonMappingException, IOException {
		if (strategyUpdatesMap.get(StrategyName) != null) {
			Map<String, StrategyUpdate> StrategyUpdateMap = strategyUpdatesMap
					.get(StrategyName);
			if (StrategyUpdateMap.get(CurrPair) != null) {
				return mapper.writeValueAsString(StrategyUpdateMap
						.get(CurrPair));
			}

		}
		return null;
	}

}
