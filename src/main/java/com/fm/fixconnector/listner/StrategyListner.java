package com.fm.fixconnector.listner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.fm.fixconnector.MessageController;
import com.fm.fixconnector.database.GenericDAO;
import com.fm.fixconnector.domain.PriceUpdate;
import com.fm.fixconnector.domain.Strategy;
import com.fm.fixconnector.repo.PriceRepo;
import com.fm.fixconnector.repo.StrategyUpdateRepo;
import com.fm.fixconnector.strategy.StrategyCalculationThread;
import com.fm.fixconnector.ui.PriceUpdateUi;
import com.fm.fixconnector.util.CustomThreadName;

public class StrategyListner implements Listner<PriceUpdate> {

	static final Logger logger = Logger.getLogger(StrategyListner.class);
	private static ExecutorService executorService;
	private List<Strategy> stratgies;
	private GenericDAO genericDAO;
	private Double quantity=(double) 1000000;
	private Double desiredProfit=(double) 100;
	private List<StrategyCalculationThread> activeThread=new ArrayList<StrategyCalculationThread>();
	private MessageController controller;
	private MessageController tradeController;
	private StrategyUpdateRepo  strategyUpdateRepo;
	PriceRepo priceRepo;
	PriceUpdateUi priceUpdateUi;
	
	public void init(){
		
		Object object=genericDAO.getObjectListForCLass(Strategy.class);
		executorService=Executors.newCachedThreadPool(new CustomThreadName("StratgeyCalcThread"));//  newFixedThreadPool(stratgies!=null?stratgies.size()+5:10);
		
		if(object!=null){
			stratgies=(List<Strategy>) object;
			for(Strategy strategy:stratgies){
				initializeStrategy(strategy);
			}
		}
	
	}
	
	public void addStratgey(Strategy strategy){
		
		stopPreviousThread(strategy);
		
		stratgies.add(strategy);
		initializeStrategy(strategy);
	}

	public void stopPreviousThread(Strategy strategy) {
		for(StrategyCalculationThread stListner:activeThread){
			if(stListner.getStrategy().getName().equalsIgnoreCase(strategy.getName())){
				logger.info("Updated in exsiting so stopping thread "+strategy.getName());
				stListner.setInactive();
				activeThread.remove(stListner);
				stratgies.remove(stListner);
				break;
			}
		}
	}
	
	public PriceUpdateUi getPriceUpdateUi() {
		return priceUpdateUi;
	}

	public void setPriceUpdateUi(PriceUpdateUi priceUpdateUi) {
		this.priceUpdateUi = priceUpdateUi;
	}

	private void initializeStrategy(Strategy strategy){
		
		StrategyCalculationThread thread=new StrategyCalculationThread(strategy,quantity,desiredProfit,controller,genericDAO,tradeController,strategyUpdateRepo,priceRepo,priceUpdateUi);
		executorService.submit(thread);
		activeThread.add(thread);
		logger.info(strategy+ " submitted for ThreadExecutuion ");
		
	}
	
	@Override
	public void onMessage(PriceUpdate update) {
		for(StrategyCalculationThread thread:activeThread){
			thread.addPriceUpdate(update);
		}
	}

	public MessageController getController() {
		return controller;
	}

	public void setController(MessageController controller) {
		this.controller = controller;
	}

	public GenericDAO getGenericDAO() {
		return genericDAO;
	}

	public void setGenericDAO(GenericDAO genericDAO) {
		this.genericDAO = genericDAO;
	}

	public MessageController getTradeController() {
		return tradeController;
	}

	public void setTradeController(MessageController tradeController) {
		this.tradeController = tradeController;
	}

	public StrategyUpdateRepo getStrategyUpdateRepo() {
		return strategyUpdateRepo;
	}

	public void setStrategyUpdateRepo(StrategyUpdateRepo strategyUpdateRepo) {
		this.strategyUpdateRepo = strategyUpdateRepo;
	}

	public PriceRepo getPriceRepo() {
		return priceRepo;
	}

	public void setPriceRepo(PriceRepo priceRepo) {
		this.priceRepo = priceRepo;
	}

}
