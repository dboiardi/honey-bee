package com.fm.fixconnector.listner;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.fm.fixconnector.MessageController;
import com.fm.fixconnector.domain.PriceUpdate;

public class UIPriceListner implements Listner<PriceUpdate>{

	static final Logger logger = Logger.getLogger(UIPriceListner.class);
	
	private MessageController messageController;
	private static ObjectMapper mapper=new ObjectMapper();
	
	
	public MessageController getMessageController() {
		return messageController;
	}
	public void setMessageController(MessageController messageController) {
		this.messageController = messageController;
	}
	
	@Override
	public void onMessage(PriceUpdate priceUpdate) {
		
		try {
			String jsonInString = mapper.writeValueAsString(priceUpdate);
			System.out.println("Following updated recevied "+priceUpdate);
			messageController.sendMessage(jsonInString);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
