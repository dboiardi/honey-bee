
package com.fm.fixconnector.strategy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;













import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.springframework.util.StopWatch;
import org.springframework.util.StringUtils;

import com.fm.fixconnector.FixMessageController;
import com.fm.fixconnector.MessageController;
import com.fm.fixconnector.analytics.QuoteQueue;
import com.fm.fixconnector.database.GenericDAO;
import com.fm.fixconnector.domain.PriceType;
import com.fm.fixconnector.domain.PriceUpdate;
import com.fm.fixconnector.domain.Strategy;
import com.fm.fixconnector.domain.StrategyCurrencyPair;
import com.fm.fixconnector.domain.StrategyUpdate;
import com.fm.fixconnector.domain.TradeReport;
import com.fm.fixconnector.initiator.ConfigManager;
import com.fm.fixconnector.listner.UIPriceListner;
import com.fm.fixconnector.repo.PriceRepo;
import com.fm.fixconnector.repo.StrategyUpdateRepo;
import com.fm.fixconnector.ui.PriceUpdateUi;
import com.fm.fixconnector.util.Constants;
import com.fm.fixconnector.util.FinanceUtil;
import com.google.common.base.Stopwatch;

public class StrategyCalculationThread extends Thread {

	static final Logger logger = Logger.getLogger(StrategyCalculationThread.class);
	private PriceUpdateUi priceupdateui;
	
	private Strategy strategy;
	private ArrayBlockingQueue<PriceUpdate> queue=new ArrayBlockingQueue<>(3);
	private ConcurrentHashMap<String, PriceUpdate> priceMap=new ConcurrentHashMap<>();
	private Double quantity=(double) 1000000;
	private Double desiredProfit=(double) 10;
	private MessageController controller;
	private static ObjectMapper objectMapper=new ObjectMapper();
	private MessageController tradeController;
	private GenericDAO dao;
	private volatile AtomicBoolean enabled=new AtomicBoolean(true);
	private StrategyUpdateRepo strategyUpdateRepo;
	private PriceRepo priceRepo;
	DateTime lastTradeTime;
	private long timeDiffInTrade=1000;
	private Stopwatch stopwatch ;
//	private StopWatch stopWatch=new StopWatch();
	
	public StrategyCalculationThread(Strategy strategy, Double quantity,
			Double desiredProfit,MessageController messageController,GenericDAO dao,MessageController tradeController,
			StrategyUpdateRepo strategyUpdateRepo,PriceRepo priceRepo,PriceUpdateUi priceUpdateUi) {
		super();
		this.strategy = strategy;
		this.quantity = quantity;
		this.desiredProfit = desiredProfit;
		this.controller=messageController;
		this.dao=dao;
		this.tradeController=tradeController;
		this.strategyUpdateRepo=strategyUpdateRepo;
		this.priceRepo=priceRepo;
		this.priceupdateui=priceUpdateUi;
	//	priceUpdateUi.sendUpdateToUi(priceUpdate, quotedValue, baseValue, priceType, startSizeBID, startSizeASK, strategyCurrencyPair, updateTime, strategy);
		
	}
	public void setInactive(){
		enabled.set(false);
	}

	public void addPriceUpdate(PriceUpdate priceUpdate){
	
		if (strategy.isActive() && strategy.getSecuritysCurrencyPair().contains(priceUpdate.getCurrencyPair().replace("_", "/"))) {
			
			logger.info("Adding price update " + priceUpdate.getCurrencyPair());
			boolean status = queue.offer(priceUpdate);
			QuoteQueue quoteQueue = new QuoteQueue(getStrategy().getName(),
					queue.size(), DateTime.now());
			dao.saveObject(quoteQueue);
			if (!status) {
				logger.error(strategy.getName() + "Strategy Queue size is "
						+ queue.size());
				queue.clear();
			}
		}
	}
	
	public void run(){
		try {
			PriceUpdate priceUpdate = queue.take();
			while (priceUpdate != null) {
				try {	
				
				priceMap.put(priceUpdate.getCurrencyPair().replace("_", "/"),priceUpdate);
				logger.info("Adding price update in Strat "+priceUpdate.getCurrencyPair()+" Price Update "+priceUpdate);
				logger.info("Last update in Strat "+priceUpdate);
				calculateBusinessLogic(priceUpdate);
				
				if(!enabled.get()){
					logger.info("Strategy stopped "+strategy.getName());
					break;
				}
				priceUpdate = queue.take();
				
				}catch(Exception exception){
					logger.error("Exception is "+strategy.getName()+ExceptionUtils.getFullStackTrace(exception));
					exception.printStackTrace();
				}
			}
			
		} catch (Exception e) {
			logger.error("Error in stratgey Queue "+e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	

	private void sendTrade(){
		
		Map map=new HashMap<>();
		for(StrategyCurrencyPair currency: strategy.getSecuritys()){
		
			map.put(55, currency.getCurrencyPair());
			map.put(54, currency.getPriceType()==PriceType.BID ? "1":"2");
			map.put(38, String.valueOf(strategy.getAmount()));
			map.put(40, String.valueOf(1));// 1 for Market Order
			tradeController.sendMessage(map);
			
		}
	}
	
	private double getValueToDivideForStart(String currencyPair,PriceType priceType){
		return priceMap.get(currencyPair)!=null?( (priceType == PriceType.BID)?priceMap.get(currencyPair).getBestBidPrice():priceMap.get(currencyPair).getBestAskPrice()):1;
	}

	
	private boolean calculateBusinessLogic(PriceUpdate priceUpdate) {
		stopwatch = Stopwatch.createStarted();
		try{
		
		double minTradeVolume=strategy.getAmount();
		
		String firstCurrency=strategy.getSecuritysCurrencyPair().get(0).split(Constants.CURR_SEPRATOR)[0];
		
		boolean retValue = false;
		double value = strategy.getAmount();
		boolean isAllPricePresent = true;
		HashMap<String,Double> priceValueMap=new HashMap<>();
		HashMap<String,Long> sizeValueMap=new HashMap<>();
		HashMap<String,String> lpValueMap=new HashMap<>();
		// Need to check for first cuurency pair beucase this would updated that value as well ????
		int i=0;
		for(StrategyCurrencyPair currency : strategy.getSecuritys()){
			PriceUpdate priceUpdate2 = priceMap.get(currency.getCurrencyPair());
			if(priceUpdate2!=null){
				double startQty=0;
				if(!StringUtils.hasText(currency.getCurrencyPairToEval())){
					
						startQty=priceUpdate2.getBestbidSize()<priceUpdate2.getBestaskSize()?priceUpdate2.getBestbidSize():priceUpdate2.getBestaskSize();
				}else{
					
					
					double askCalculated;
					double bidCalculated;
					if( currency.getEvalPriceType()!=null && PriceType.BID.name().equalsIgnoreCase(currency.getEvalPriceType().name())){
						askCalculated = priceUpdate2.getBestaskSize()*getValueToDivideForStart(currency.getCurrencyPairToEval(), currency.getEvalPriceType());
						bidCalculated = priceUpdate2.getBestbidSize()*getValueToDivideForStart(currency.getCurrencyPairToEval(), currency.getEvalPriceType());
					}
					else {
						askCalculated = priceUpdate2.getBestaskSize()/getValueToDivideForStart(currency.getCurrencyPairToEval(), currency.getEvalPriceType());
						bidCalculated = priceUpdate2.getBestbidSize()/getValueToDivideForStart(currency.getCurrencyPairToEval(), currency.getEvalPriceType());
						
							
					}
					
					startQty=bidCalculated<askCalculated?bidCalculated:askCalculated;	
					
				}
				
				if(startQty!=0 && value>startQty){
					value=startQty;
				}	
			}else{
				logger.error("Not Calculating becuase All currency pair is not present");
				break;
			}
			i++;
		}
		double minVolume=value*2.5;
		logger.info("Minimum value used "+value);
		i=0;
		for (StrategyCurrencyPair currency : strategy.getSecuritys()) {
			
			PriceUpdate priceUpdate2 = priceMap.get(currency.getCurrencyPair());
			if (priceUpdate2 == null ||(priceUpdate2!=null && (priceUpdate2.getBestBidPrice()==0|| priceUpdate2.getBestAskPrice()==0))) {
				// Not enough currency pair so breaking;
				isAllPricePresent = false;
				break;
				// Debug logs
			} else {

				if (currency.getPriceType() == PriceType.ASK)  {
					priceValueMap.put(currency.getCurrencyPair(),priceUpdate2.getBestAskPrice() );
					sizeValueMap.put(currency.getCurrencyPair(),priceUpdate2.getAskSize());
					lpValueMap.put(currency.getCurrencyPair(),priceUpdate2.getBestASkLP());
					
					if(priceUpdate2.getBestAskPrice()!=0){
						
						double newvalue = value / priceUpdate2.getBestAskPrice();
						double startQtyBid;
						double startQtyAsk;
						if(!StringUtils.hasText(currency.getCurrencyPairToEval())){
							startQtyBid=priceUpdate2.getBestbidSize();
							 startQtyAsk=priceUpdate2.getBestaskSize();
						}else{
							if(currency.getEvalPriceType()==PriceType.BID)	{
							startQtyBid=priceUpdate2.getBestbidSize()*getValueToDivideForStart(currency.getCurrencyPairToEval(), currency.getEvalPriceType());
							 startQtyAsk=priceUpdate2.getBestaskSize()*getValueToDivideForStart(currency.getCurrencyPairToEval(), currency.getEvalPriceType());
							}else {
								startQtyBid=priceUpdate2.getBestbidSize()/getValueToDivideForStart(currency.getCurrencyPairToEval(), currency.getEvalPriceType());
								 startQtyAsk=priceUpdate2.getBestaskSize()/getValueToDivideForStart(currency.getCurrencyPairToEval(), currency.getEvalPriceType());
									
							}
						}
						
						priceupdateui.sendUpdateToUi(priceUpdate2, newvalue, value,currency.getPriceType(),startQtyBid,startQtyAsk,currency,priceUpdate.getDateTime(),strategy);
					
							value = newvalue;
					}
					
										

				} else {
					priceValueMap.put(currency.getCurrencyPair(),priceUpdate2.getBestBidPrice());
					sizeValueMap.put(currency.getCurrencyPair(),priceUpdate2.getBidSize());
					lpValueMap.put(currency.getCurrencyPair(),priceUpdate2.getBestBidLP());
					double newvalue = value * priceUpdate2.getBestBidPrice();
					double startQtyBid;
					double startQtyAsk;
					
					if(!StringUtils.hasText(currency.getCurrencyPairToEval())){
						startQtyBid=priceUpdate2.getBestbidSize();
						startQtyAsk=priceUpdate2.getBestaskSize();
					}else{
						if(currency.getEvalPriceType()==PriceType.BID)	{
							startQtyBid=priceUpdate2.getBestbidSize()*getValueToDivideForStart(currency.getCurrencyPairToEval(), currency.getEvalPriceType());
							 startQtyAsk=priceUpdate2.getBestaskSize()*getValueToDivideForStart(currency.getCurrencyPairToEval(), currency.getEvalPriceType());
							}else {
								startQtyBid=priceUpdate2.getBestbidSize()/getValueToDivideForStart(currency.getCurrencyPairToEval(), currency.getEvalPriceType());
								 startQtyAsk=priceUpdate2.getBestaskSize()/getValueToDivideForStart(currency.getCurrencyPairToEval(), currency.getEvalPriceType());
									
							}
					}
					priceupdateui.sendUpdateToUi(priceUpdate2, newvalue, value, currency.getPriceType(), startQtyBid, startQtyAsk, currency, priceUpdate.getDateTime(), strategy);
					
					value = newvalue;
					
					
				}
			}
			i++;
		}
		
		if (isAllPricePresent) {
			desiredProfit=ConfigManager.getConfig().getProfit();
			if ((value - minVolume) >= desiredProfit) {
				retValue = true;
				logger.info(strategy.getName()
						+ " Strategy qualified for placing Trade becuase Value "
						+ value);
				DateTime now=DateTime.now();
				
				if(lastTradeTime==null || (lastTradeTime!=null  && now.getMillis()-lastTradeTime.getMillis() >= timeDiffInTrade)){	
					
				lastTradeTime=DateTime.now();
				double profitInReportingCurrency = 0;
				String sellingCurr=FinanceUtil.getSellingCurrency(strategy.getReportingCurrencyPair(), strategy.getReportingCurrencypriceType());
				
				
				if(!StringUtils.hasText(strategy.getReportingCurrencyPair()) || strategy.getSecuritys().get(0).getFromCurrency().equalsIgnoreCase(strategy.getReportingCurrency())){
					profitInReportingCurrency=(value-minVolume);
				}else{
				
				if(StringUtils.hasText(strategy.getReportingCurrencyPair()) &&  strategy.getReportingCurrencypriceType()!=null){
					
					double price=priceRepo.getPriceForCuurencyAsPerPriceType(strategy.getReportingCurrencyPair(), strategy.getReportingCurrencypriceType());
					
					if(strategy.getReportingCurrencypriceType()==PriceType.BID){
						profitInReportingCurrency=price*(value-minVolume);
					}else if(strategy.getReportingCurrencypriceType()==PriceType.ASK){
						profitInReportingCurrency=(value-minVolume)/price;
					}else{
						logger.error("no matching price type set for strategy "+strategy.getName());
					}
				}else{
					logger.error("Reporting currecny is not set for strategy "+strategy.getName());
				
				}
				}
				
				TradeReport report=new TradeReport(strategy.getName(), new DateTime(), (value - minVolume), desiredProfit, priceValueMap,sizeValueMap,lpValueMap,strategy.getReportingCurrency(),profitInReportingCurrency);
				
				dao.saveObject(report);
				/*if(isAllRiskMeasureMeet()){
					sendTrade();
				}*/
				}else{
					logger.info("Not placing trade becuase Risk meausre not meet .."+strategy.getName());	
				}
				
			}
		}
		
	//	logger.info("Avg Time Taken "+(stopWatch.getTotalTimeMillis()/stopWatch.getTaskCount())+" Last task "+stopWatch.getLastTaskTimeMillis());
		return retValue;
	
		}catch(Exception exception){
			logger.error("Error occured "+ExceptionUtils.getFullStackTrace(exception));
			return false;
		}finally{
			logger.info(strategy.getName()+" Elapsed time in Microseconds ==> " + stopwatch.elapsed(TimeUnit.MICROSECONDS));
		}
		
	}
	
	
	private boolean isAllRiskMeasureMeet() {
		boolean result =true;
		for(StrategyCurrencyPair currencyPair:strategy.getSecuritys()){
			int refRate=priceRepo.getUpdateInLastSecond(currencyPair.getCurrencyPair());
			
			if(currencyPair.getRefreshRate() <= refRate){
				result=false;
				logger.info("Not placing trade becuase "+ currencyPair.getCurrencyPair()+" refresh rate "+refRate +" is higer then the allowed refRate"+currencyPair.getRefreshRate());
				break;
			
			}
		}
		return result;
	}
	public Strategy getStrategy() {
		return strategy;
	}
	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}
	
	
	
}
