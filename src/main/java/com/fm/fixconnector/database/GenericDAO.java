package com.fm.fixconnector.database;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.mapreduce.GroupBy;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.util.StringUtils;

import com.fm.fixconnector.domain.PriceType;
import com.fm.fixconnector.domain.PriceUpdate;
import com.fm.fixconnector.domain.SecurityList;
import com.fm.fixconnector.domain.Strategy;
import com.fm.fixconnector.domain.StrategyUpdate;
import com.fm.fixconnector.domain.TradeableSecurity;
import com.fm.fixconnector.nordea.ExecutionReportNORDEA;
import com.fm.fixconnector.nordea.MarketData;
import com.fm.fixconnector.seb.ExecutionReportSEB;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

public class GenericDAO {
	
	ArrayBlockingQueue<Object> queue=new ArrayBlockingQueue<Object>(1000);
	Thread thread;
	
	public void init(){
		thread=new Thread(new Runnable() {
			
			@Override
			public void run() {
				try{
					Object obj=queue.take();
					while(obj!=null){
						mongoTemplate.save(obj);
						obj=queue.take();
					}
				
				}catch(Exception exception){
					logger.error("Error in monogThread"+ExceptionUtils.getFullStackTrace(exception));
				}
			}
		});
		thread.setName("MONGODB_THREADE");
		thread.start();
	}

	static final Logger logger = Logger.getLogger(GenericDAO.class);
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private ObjectMapper mapper=new ObjectMapper();

	private String configCollectionName = "trade";

	List<Class> classes = new ArrayList<Class>();

	public String getConfigCollectionName() {
		return configCollectionName;
	}

	public void setConfigCollectionName(String configCollectionName) {
		this.configCollectionName = configCollectionName;
	}

	public MongoTemplate getMongoTemplate() {
		return mongoTemplate;
	}

	public void setMongoTemplate(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;

	}

	public Object getAllObject() {
		List<Object> trades = new ArrayList<Object>();
		for (Class className : classes) {
			trades.addAll(mongoTemplate.findAll(className));
		}
		return trades;
	}

	public Object getAllObjectWithTradeType(String tradeType) {
		List<Object> trades = new ArrayList<Object>();
		Query mongoQuery = new Query();
		mongoQuery.addCriteria(Criteria.where("tradeType").is(tradeType));
		for (Class className : classes) {
			trades.addAll(mongoTemplate.find(mongoQuery, className));
		}
		return trades;
	}

	public Object saveObject(Object object) {
		logger.debug("Saving Object " + object);
	//	mongoTemplate.save(object);
		try {
			queue.put(object);
			logger.info("Queue Size in Mongo "+queue.size());
		} catch (InterruptedException e) {
		
			e.printStackTrace();
		}
		return object;
	}
	
	public void saveStartgeyUpdate(String xml){
	try {
		saveObject(	mapper.readValue(xml, StrategyUpdate.class));
	} catch (IOException e) {
		e.printStackTrace();
	}

	}
	
	public Object getObject(Class clasType){
		return mongoTemplate.findAll(clasType);
	}
	
	public Object getObjectListForCLass(Class className) {
		return	mongoTemplate.findAll(className);
	}
	
	public int deleteStrategy(String strategyName){

		Query mongoQuery = new Query();
		mongoQuery.addCriteria(Criteria.where("name").is(strategyName));
		WriteResult result=	mongoTemplate.remove(mongoQuery, Strategy.class);
		return 1;
		
	}
	
	public void saveTradeEableSecurites(SecurityList securityList) {
		for(TradeableSecurity tradeableSecurity:securityList.getSecuritys()){
			if(getTradeEableSecurityforInstrument(tradeableSecurity.getSymbol())==null){
				logger.info("Saving TradeEableSecurity " + tradeableSecurity);
				mongoTemplate.save(tradeableSecurity);
			}else{
				logger.info("Not saving "+tradeableSecurity);	
			}	
		}
	}
	
	public TradeableSecurity getTradeEableSecurityforInstrument(String instrument){
		
		Query mongoQuery = new Query();
		
		mongoQuery.addCriteria(Criteria.where("symbol").is(instrument));
		
		TradeableSecurity trSecurity=mongoTemplate.findOne(mongoQuery, TradeableSecurity.class);
		
		return trSecurity;
	}
	
	public Strategy getStrategy(String stratgey){
		
		Query mongoQuery = new Query();
		
		mongoQuery.addCriteria(Criteria.where("name").is(stratgey));
		
		Strategy strategy=mongoTemplate.findOne(mongoQuery, Strategy.class);
		
		return strategy;
	}
	
	public List<MarketData> getMarketDataForLastNSecond(String currencyPair,int lastNSecond,DateTime endTime,PriceType priceType){
	
		Query mongoQuery = new Query();
		
		String priceTypeValue=PriceType.BID==priceType?"0":"1";//
		mongoQuery.addCriteria(Criteria.where("clientTime").gte(endTime.minusSeconds(lastNSecond)).lte(endTime).andOperator(Criteria.where("currency").is(currencyPair),(Criteria.where("priceType").is(priceTypeValue))));
		mongoQuery.with(new Sort(Direction.ASC,"clientTime"));
		List<MarketData> marketDatas=mongoTemplate.find(mongoQuery, MarketData.class);
		return marketDatas;
		
	}
	
	public List<PriceUpdate> getPriceUpdateForLastNSecond(String currencyPair,int lastNSecond,DateTime endTime){
		
		Query mongoQuery = new Query();
		
		
		mongoQuery.addCriteria(Criteria.where("dateTime").gte(endTime.minusSeconds(lastNSecond)).lte(endTime).andOperator(Criteria.where("currencyPair").is(currencyPair)));
		mongoQuery.with(new Sort(Direction.ASC,"dateTime"));
		List<PriceUpdate> priceUpdates=mongoTemplate.find(mongoQuery, PriceUpdate.class);
		return priceUpdates;
		
	}

}
