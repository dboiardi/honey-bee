package com.fm.fixconnector.util;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.fm.fixconnector.nordea.MarketData;

public class CommanFunction {

	public static long getDateDiffFromNow(Date date1, TimeUnit timeUnit) {
		Date date2=new Date();
	    long diffInMillies = date2.getTime() - date1.getTime();
	    return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	}
	
	public static boolean isBidData(MarketData data){
		if("0".equalsIgnoreCase(data.getPriceType())){
			return true;
		}else{
			return false;
		}
	}
}
