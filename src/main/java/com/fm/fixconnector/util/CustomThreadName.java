package com.fm.fixconnector.util;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class CustomThreadName implements ThreadFactory {

	private String name;
	private AtomicInteger atomicInteger=new AtomicInteger();
	
	public CustomThreadName(String name) {
		super();
		this.name = name;
	}


	@Override
	public Thread newThread(Runnable r) {
		Thread thread= new Thread(r);
		thread.setName(name+atomicInteger.getAndIncrement());
		return thread;
	}

}
