package com.fm.fixconnector.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.fm.fixconnector.algo.OrderGateway;
import com.fm.fixconnector.database.GenericDAO;
import com.fm.fixconnector.domain.AbstractTradeCapture;

//Provide jmx interface
@ManagedResource
public class Admin {

	@ManagedOperation
	public void setNoOfTrade(int noOfTrade){
		OrderGateway.noOfMaxTrade=noOfTrade;
	}
	
	private GenericDAO dao;
	
	public GenericDAO getDao() {
		return dao;
	}
	
	public void setDao(GenericDAO dao) {
		this.dao = dao;
	}
	
	/*@ManagedOperation
	public String getAllTrades(){
	return getHtmlForTrades(dao.getAllObject());
	}
	
	@ManagedOperation
	public Object getAllTradesForType(String type){
		return (dao.getAllObjectWithTradeType(type));
	}
	
	@ManagedOperation
	public Object getAllTradesBetweenDate(Date d1,Date d2){
		
		return filterEligibleTrade((List<AbstractTradeCapture>)dao.getAllObject(), d1, d2);
	}
	
	@ManagedOperation
	public Object getAllTradesBetweenDateForType(Date d1,Date d2,String type){
		return filterEligibleTrade((List<AbstractTradeCapture>)dao.getAllObjectWithTradeType(type), d1, d2);
	}
*/	
	private String getHtmlForTrades(Object object){
		StringBuffer result=new StringBuffer();
		List<AbstractTradeCapture> tardes=(List<AbstractTradeCapture>)object;
		result.append("<table>");
		for(AbstractTradeCapture tradeCapture:tardes){
			result.append("<tr>");
			result.append("<td>");result.append(tradeCapture.getOrderId());result.append("</td>");
			result.append("<td>");result.append(tradeCapture.getPrice());result.append("</td>");
			result.append("<td>");result.append(tradeCapture.getQunantity());result.append("</td>");
			result.append("<td>");result.append(tradeCapture.getTradeDate());result.append("</td>");
			result.append("<td>");result.append("");result.append("</td>");
			result.append("</tr>");
		}
		result.append("</table>");
		return result.toString();
	}
	private List filterEligibleTrade(List<AbstractTradeCapture> trades,Date d1,Date d2){ 
		List eligibleTrade=new ArrayList<AbstractTradeCapture>();
		for(AbstractTradeCapture tradeCapture:trades){
			if(tradeCapture.getTradeDate().after(d1) && tradeCapture.getTradeDate().before(d2)){
				eligibleTrade.add(tradeCapture);
			}}
		return eligibleTrade;
		}
}
