package com.fm.fixconnector.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	private static  SimpleDateFormat formatter=new SimpleDateFormat("yyyyMMdd-HH:mm:ss.SSS");
	
	public static Date getFormatedSendingTime(String time) {
		try {
			return formatter.parse(time);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
}
