package com.fm.fixconnector.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.fm.fixconnector.domain.AbstractTradeCapture;
import com.fm.fixconnector.domain.TradeExecutionReport;
import com.fm.fixconnector.domain.TradeObject;
import com.mongodb.BasicDBObject;

public class MailSender {

	private JavaMailSender mailSender;
	private VelocityEngine velocityEngine;
	private String mailFrom;
	private String templateLoc;
	private String[] toMail = {"bhavishyagoyal@gmail.com"};
	static final Logger logger = Logger.getLogger(MailSender.class);

	
	public String[] getToMail() {
		return toMail;
	}

	public void setToMail(String[] toMail) {
		this.toMail = toMail;
	}

	public String getTemplateLoc() {
		return templateLoc;
	}

	public void setTemplateLoc(String templateLoc) {
		this.templateLoc = templateLoc;
	}

	public JavaMailSender getMailSender() {
		return mailSender;
	}

	public VelocityEngine getVelocityEngine() {
		return velocityEngine;
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}

	public String getMailFrom() {
		return mailFrom;
	}

	ExecutorService executorService = Executors.newFixedThreadPool(1);
	ArrayBlockingQueue<Object> blockingQueue = new ArrayBlockingQueue<Object>(100);


	public void init() {/*
		logger.info("Mail Thread Started ..");
		executorService.execute(new Runnable() {
			public void run() {
				try {
					Object tradeObj = blockingQueue.take();
					do {
						sendMailOn(tradeObj);
						tradeObj = blockingQueue.take();
					} while (tradeObj != null);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		});
*/	}

	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}

	public Object sendMail(Object tradeObj) {
		blockingQueue.add(tradeObj);
		return tradeObj;
	}

	public void sendMailOn(final Object tradeObj) {

		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
				message.setTo(toMail);
				message.setFrom(mailFrom); // could be parameterized...
				message.setSubject("Trade Report ");
				message.setBcc(mailFrom);
				Map model = new HashMap();
				model.put("trade", (TradeExecutionReport) tradeObj);
				String text = VelocityEngineUtils.mergeTemplateIntoString(
						velocityEngine, templateLoc, model);
				message.setText(text, true);
			}
		};
		mailSender.send(preparator);
		logger.info("Mail has beensent successfuly");
	}

}
