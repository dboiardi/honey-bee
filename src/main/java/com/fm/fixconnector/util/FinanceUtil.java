package com.fm.fixconnector.util;

import org.springframework.util.StringUtils;

import com.fm.fixconnector.domain.PriceType;

public class FinanceUtil {

	public static String getReportingCurrency(String currecnyPair,
			PriceType priceType) {
		if (StringUtils.hasText(currecnyPair) && priceType != null) {
			String[] currPair = currecnyPair.replaceAll("-", "/").split("/");

			if (currPair != null && currPair.length == 2) {

				if (priceType == PriceType.BID) {
					return currPair[1];
				} else if (priceType == PriceType.ASK) {
					return currPair[0];
				}

			}
		}
		return "";
	}

	public static String getSellingCurrency(String currecnyPair,
			PriceType priceType) {

		if (StringUtils.hasText(currecnyPair) && priceType != null) {
			String[] currPair = currecnyPair.replaceAll("-", "/").split("/");

			if (currPair != null && currPair.length == 2) {

				if (priceType == PriceType.BID) {
					return currPair[0];
				} else if (priceType == PriceType.ASK) {
					return currPair[1];
				}

			}
		}
		return "";
	}
}
