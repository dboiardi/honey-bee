package com.fm.fixconnector;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Producer;
import org.apache.camel.component.quickfixj.MessagePredicate;
import org.apache.camel.component.quickfixj.QuickfixjProducer;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.fm.fixconnector.algo.OrderGateway;
import com.fm.fixconnector.domain.AbstractMarketData;
import com.fm.fixconnector.nordea.MarketData;

import quickfix.Message;
import quickfix.SessionID;
import quickfix.field.MsgType;

public class FixMessageController extends MessageController {

	static final Logger logger = Logger.getLogger(FixMessageController.class);
	MessageRepo messageRepo;
	String senderCompId;
	String targetCompId;
	String messageVersion;

	public MessageRepo getMessageRepo() {
		return messageRepo;
	}

	public void setMessageRepo(MessageRepo messageRepo) {
		this.messageRepo = messageRepo;
	}

	public String getSenderCompId() {
		return senderCompId;
	}

	public void setSenderCompId(String senderCompId) {
		this.senderCompId = senderCompId;
	}

	public String getTargetCompId() {
		return targetCompId;
	}

	public void setTargetCompId(String targetCompId) {
		this.targetCompId = targetCompId;
	}

	public String getMessageVersion() {
		return messageVersion;
	}

	public void setMessageVersion(String messageVersion) {
		this.messageVersion = messageVersion;
	}

	public void init() {
		super.init();

	}

	public void sendSecurityListMessage(Map<Integer, String> valueMap) {

		Message message  = messageRepo.getSecurityListMessage(valueMap);
		sendMessage(message);
		
	}

	public void sendPriceRequestMessage(Map<Integer, String> valueMap) {

		Message message = messageRepo.getPriceStreamMessage(valueMap);
		sendMessage(message);
	}

	public void SendMessage(Message message) {
		// Message message=messageRepo.getPlaceOrderMessage(valueMap);
		sendMessage(message);
	}

	public void sendOrderRequestMessage(Map<Integer, String> valueMap) {

		Message message = messageRepo.getOrderMessage(valueMap);
		sendMessage(message);
	}

	public void sendMessage(Object message) {

		try {
			Exchange exchange = null;
			producer = gatewayEndpoint.createProducer();
			exchange = producer.createExchange(ExchangePattern.InOut);
			logger.info("Sending FIX message "+message);
			exchange.getIn().setBody(message);
			exchange.setProperty(QuickfixjProducer.CORRELATION_CRITERIA_KEY,
					new MessagePredicate(new SessionID(messageVersion,
							targetCompId, senderCompId), MsgType.LOGON));
			producer.process(exchange);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Following exception  "+e.getMessage()+e.getStackTrace());
		}

	}

}
