package com.fm.fixconnector.initiator;

import java.util.List;

import org.apache.log4j.Logger;

import com.fm.fixconnector.database.GenericDAO;
import com.fm.fixconnector.domain.SystemConfiguration;

public class ConfigManager {

	private static SystemConfiguration configuration=new SystemConfiguration(10,5);
	private GenericDAO genericDAO;
	
	static final Logger logger = Logger.getLogger(ConfigManager.class);
	
	public void init(){
		Object obj=genericDAO.getObject(SystemConfiguration.class);
		if(obj!=null  && obj instanceof List  && ((List)obj).size()==1){
			configuration= ((List<SystemConfiguration>)obj).get(0);
			
		}else{
			logger.info("No Configuration found in db ..");
			genericDAO.saveObject(configuration);
		}
		
		logger.info("Following configuration would be used .. "+configuration);
	}
	
	public static SystemConfiguration getConfig(){
		return configuration;
	}

	public static SystemConfiguration getConfiguration() {
		return configuration;
	}

	public static void setConfiguration(SystemConfiguration configuration) {
		ConfigManager.configuration = configuration;
	}

	public GenericDAO getGenericDAO() {
		return genericDAO;
	}

	public void setGenericDAO(GenericDAO genericDAO) {
		this.genericDAO = genericDAO;
	}
	
	
}
